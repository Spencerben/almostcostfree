<?php include 'header.php'; ?>



<!-- Start of  help -->

<div class="container"><!-- Start  container -->

    <div class="row"><!-- Start  row -->

        <h2 class="text-center red">Help / FAQ</h2>

        <hr class="col-xs-12 col-md-12"  style="border-top: 1px solid green !important;" />

        <div class="row ol-xs-12 col-md-12">

            <div class="tab" role="tabpanel"> <!-- Start class="tab" role="tabpanel" -->


            <div class="col-xs-12 col-md-4">
            
            <!-- Nav tabs -->
             <ul class="nav nav-tabs vertical-menu" role="tablist">
                    
                <li class="vertical-menu active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Company formation</a></li>
                <li class="vertical-menu"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Domain name</a></li>
                <li class="vertical-menu"><a href="#Section3" aria-controls="message" role="tab" data-toggle="tab">Web design</a></li>
                <li class="vertical-menu"><a href="#Section4" aria-controls="setting" role="tab" data-toggle="tab">Company services</a></li>
               
              
             </ul>

           </div>


            <div class="col-xs-12 col-md-8">

                <!-- Tab pane -->
                <div class="tab-content">



                    <div role="tabpanel" class="row tab-pane active" id="Section1">

                       <div class="row">  <!-- start row -->    

                        <div class="col-xs-12 col-md-12"><!-- Start  col-md-12 -->

                                             
                            <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                                   
                                <h3 class="text-center">Company formation</h3>


                                 <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1a" aria-expanded="false" aria-controls="collapse1a">
                                                                        <span> </span>
                                                                        <p class="red">How do I register company?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse1a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1a">
                                                                <div class="panel-body">
                                                                    
                                                                    <li>You can register online or by post</li>

                                                                    <li>we recommended you to register online because more cheaper and fast(4 hours)</li>

                                                                    <li>Postal applications take 8 to 10 days and cost £40 (paid by cheque made out to ‘Companies House’).</li>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse2a" aria-expanded="false" aria-controls="collapse2a">
                                                                        <span> </span>
                                                                       <p class="red">how long will take time to register?</p> 
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse2a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2a">
                                                                <div class="panel-body">
                                                                    <li>Your company is usually registered within 4 hours.</li>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse3a" aria-expanded="false" aria-controls="collapse3a">
                                                                        <span> </span>
                                                                        <p class="red">What do I need inorder to register private limited company?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3a">
                                                                <div class="panel-body">
                                                                    <h4>You’ll need:</h4>

                                                                    <li>a suitable company name</li>
                                                                    <li>an address for the company</li>
                                                                    <li>at least one director</li>
                                                                    <li>details of the company’s shares - you need at least one shareholder</li>
                                                                    <li>to check what your SIC code is - this identifies what your company does</li>
                                                                    <h4>You’ll also need:</h4>
                                                                    <li>shareholders to agree to create the company and the written rules (known as ‘memorandum and articles of association’)</li>
                                                                    <li>details of people with significant control over your company, for example anyone with more than 25% shares or voting rights</li>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse4a" aria-expanded="false" aria-controls="collapse4a">
                                                                        <span> </span>
                                                                        <p class="red">How do I choose a company name?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4a">
                                                                <div class="panel-body">
                                                                    <li>Your name can’t be the same as another registered company’s name. If your name is too similar to another company’s name you may have to change it if someone makes a complaint.</li>


                                                                    <li>Your name must usually end in either ‘Limited’ or ‘Ltd’. You can include the Welsh equivalents ‘Cyfyngedig’ and ‘Cyf’ instead if you registered the company in Wales.</li>

                                                                    <li>Your company name can’t be offensive.</li>

                                                                    <li>Your name also can’t contain a ‘sensitive’ word or expression, or suggest a connection with government or local authorities, unless you get permission.</li>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse5a" aria-expanded="false" aria-controls="collapse5a">
                                                                        <span> </span>
                                                                        <p class="red">When don’t you have to use ‘limited’ in your company name</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5a">
                                                                <div class="panel-body">
                                                                    <p>You don’t have to use ‘limited’ in your name if your company is a registered charity or limited by guarantee and your articles of association say your company:</p>

                                                                    <li>promotes or regulates commerce, art, science, education, religion, charity or any profession</li>
                                                                    <li>can’t pay its shareholders, for example through dividends</li>
                                                                    <li>requires each shareholder to contribute to company assets if it’s wound up during their membership, or within a year of them stopping being a shareholder</li>
                                                            
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse6a" aria-expanded="false" aria-controls="collapse6a">
                                                                        <span> </span>
                                                                        <p class="red">What does registered office address mean?</p>
                                                                    </a>
                                                                </h4>
                                                         </div>
                                                        <div id="collapse6a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6a">
                                                                <div class="panel-body">
                                                                    <h5>Your registered office address is where official communications will be sent, for example letters from Companies House.</h5>
                                                                    <h5>The address must be:</h5>
                                                                    <li>a physical address in the UK</li>
                                                                    <li>in the same country your company is registered in, for example a company registered in Scotland must have a registered office address in Scotland</li>
                                                                    <br>
                                                                    <h5>You can use a PO Box. You must still include a physical address and postcode.</h5>
                                                                    <h5>You can use your home address or the address of the person who will manage your Corporation Tax.</h5>
                                                                    <h5>Your company address will be publicly available on the online register.</h5>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading7a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse7a" aria-expanded="false" aria-controls="collapse7a">
                                                                        <span> </span>
                                                                        <p class="red">what is Director's responsiblity?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse7a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7a">
                                                                <div class="panel-body">
                                                                    <p>Directors are legally responsible for running the company and making sure company accounts and reports are properly prepared.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse8a" aria-expanded="false" aria-controls="collapse8a">
                                                                        <span> </span>
                                                                        <p class="red">What is the minimum age for director?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse8a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8a">
                                                                <div class="panel-body">
                                                                    <p>A director must be 16 or over and not be disqualified from being a director.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse9a" aria-expanded="false" aria-controls="collapse9a">
                                                                        <span> </span>
                                                                        <p class="red">Do the directors have to live in the UK?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9a">
                                                                <div class="panel-body">
                                                                    <p>Directors don’t have to live in the UK but companies must have a UK registered office address.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10a">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse10a" aria-expanded="false" aria-controls="collapse10a">
                                                                        <span> </span>
                                                                       <p class="red">Does private limited company need secretaries?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse10a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10a">
                                                                <div class="panel-body">
                                                                    <p>You don’t need a company secretary for a private limited company. Some companies use them to take on some of the director’s responsibilities.</p>
                                                                    <p>The company secretary can be a director but can’t be:</p>
                                                                    <li>the company’s auditor</li>
                                                                    <li>an ‘undischarged bankrupt’ - unless they have permission from the court</li>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        

                         

                            </div>

                        </div> <!-- end  col-md-12 -->

                       </div> <!-- end row -->

                    </div><!-- end  Section 1 -->



                    <!-- Start  Section 2 -->
                    <div role="tabpanel" class="row tab-pane" id="Section2">

                     <div class="row">  <!-- start row --> 

                        <div class="col-xs-12 col-md-12"><!-- Start  col-md-12 -->

                                             
                            <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                                   

                                <h3 class="text-center">Domain name</h3>


                                <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse1b" aria-expanded="false" aria-controls="collapse1b">
                                                                        <span> </span>
                                                                        <p class="red">What is a domain name?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse1b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1b">
                                                                <div class="panel-body">
                                                                    <p>A domain name like www.yourdomainname.com is a website address that people will use to find you on the internet. Domain names are used as an address to locate businesses, organizations, or other entities on the Internet.</p>

                                                                    <p>The domain name is the foundation of your website (www.yourdomainname.com) as well as your email address (info@yourdomainname.com), using a well thought up domain name allows your customers to identify and remember you more easily on the Web.</p>

                                                                    <p>A great benefit of using your own domain name is that regardless of how many times you change your website hosting provider, your Email and website address will always remain the same.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2b" aria-expanded="false" aria-controls="collapse2b">
                                                                        <span> </span>
                                                                        <p class="red">Can I have a website without a domain name?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse2b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2b">
                                                                <div class="panel-body">
                                                                    <p>You can not have a website without a domain name. As with home addresses, domains help visitors to access your website.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse3b" aria-expanded="false" aria-controls="collapse3b">
                                                                        <span> </span>
                                                                        <p class="red">I don't have a website yet, why do I need a domain name now?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3b">
                                                                <div class="panel-body">
                                                                    <p>Registering your domain name now ensures that you own the domain name that may not otherwise be available when you want it.</p>


                                                                    <p>Once registered, the domain name is yours, provided the domain renewal fees are paid each year.</p>


                                                                    <p>You can park the domain, use it, sell it or lease it to other individuals or companies.</p>


                                                                    <p> Domain parking is included free with all our domain name registrations and transfers.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse4b" aria-expanded="false" aria-controls="collapse4b">
                                                                        <span> </span>
                                                                     <p class="red">What type of domain names can I register at almostcostfree.com ?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4b">
                                                                <div class="panel-body">
                                                                    <p>We offer all Uk ccTLD and gTLD domains (ie: .co.uk .org.uk .net.uk .web.uk and .london)</p>


                                                                    <p>International TLD's and gTLD's domains are also available, including all the most popular .com .net .org .biz and .mobi.</p>


                                                                    <p> With more than 1000 new gTLD domains being added to the internet, we will constantly update our list of available domain extensions.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse5b" aria-expanded="false" aria-controls="collapse5b">
                                                                        <span> </span>
                                                                        <p class="red">Why almostcostfree.com for your domain names</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5b">
                                                                <div class="panel-body">
                                                                    
                                                                    <p>With each domain name registration or transfer we include a range of free services including Domain Parking, URL Forwarding and DNS Management on our globally redundant nameservers</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse6b" aria-expanded="false" aria-controls="collapse6b">
                                                                        <span> </span>
                                                                        <p class="red">How long does it take to register a domain name?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6b">
                                                                <div class="panel-body">
                                                                 <p>Your domain registration could be complete within minutes, provided the domain registration fee is paid and your registration information is filled out correctly.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading7b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse7b" aria-expanded="false" aria-controls="collapse7b">
                                                                        <span> </span>
                                                                        <p class="red">What is the maximum length my domain name can be?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse7b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7b">
                                                                <div class="panel-body">
                                                                    <p>63 characters is the maximum number of characters for a domain name (excluding the extension).</p>
                                                                    <p>A valid domain name can have a minimum of 2 and a maximum of 63. You can use any combination of the 26 letters in English alphabet, numbers from 0 to 9 and the "-" (dash) character.</p>
                                                                    <p>It is generally a good idea to have a short domain name so that it is easier for customers/visitors to type and remember.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse8b" aria-expanded="false" aria-controls="collapse8b">
                                                                        <span> </span>
                                                                        <p class="red">Can a domain name be ordered without any web hosting?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse8b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8b">
                                                                <div class="panel-body">
                                                                    <p>Yes, you are welcome to purchase a domain name without website hosting.</p>

                                                                    <p>You are free to use that domain name with your hosting elsewhere, or you can purchase hosting with AlmostCostFree at a later time and connect your domain name to your hosting. Or if you just require the domain name for security, you can leave it blank for as long as you like.</p>

                                                                    <p>You can also use your domain name that's registered with a third party with your AlmostCostFree web hosting.</p>

                                                                    <p>It's possible to have either domain name and website hosting with us, or "one and not the other" as well.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse9b" aria-expanded="false" aria-controls="collapse9b">
                                                                        <span> </span>
                                                                        <p class="red">Can I have multiple domain names point to the same website?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9b">
                                                                <div class="panel-body">
                                                                     <p>Yes. This service is referred to as a "Parked Domain" or "Add-on Domain"</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10b">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse10b" aria-expanded="false" aria-controls="collapse10b">
                                                                        <span> </span>
                                                                        <p class="red">How long does it take for my domain to become available?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse10b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10b">
                                                                <div class="panel-body">
                                                                    <p>Once the Domain Name is registered, it usually takes a couple of minutes before you see your domain name activated on our servers but can take from 24-48 hours before the name "spreads" around the Internet so that all Domain Name Servers know about it.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            

                         

                            </div>

                        </div> <!-- end  col-md-12 -->

                      </div> <!-- end row -->

                    </div><!-- end  Section 2 -->




                    <!-- Start  Section 3 -->
                    <div role="tabpanel" class="row tab-pane" id="Section3">

                      <div class="row">   <!-- start row -->            

                        <div class="col-xs-12 col-md-12"><!-- Start  col-md-12 -->

                                             
                            <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                                                   

                                <h3 class="text-center">Web design</h3>


                                <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1c">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse1c" aria-expanded="true" aria-controls="collapse1c">
                                                                        <span> </span>
                                                                        <p class="red">I am totally new to this "website thing". How does the whole process work?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse1c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1c">
                                                                <div class="panel-body">
                                                                    <p>Never fear, that's why we are here. You can learn about our time proven process in ourprocess section.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse2c" aria-expanded="false" aria-controls="collapse2c">
                                                                        <span> </span>
                                                                        <p class="red">How Long Does It Take To Create A Website?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse2c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2c">
                                                                <div class="panel-body">
                                                                    <p>The length of time it takes to create a fully functional website is based on several factors. Custom design work can take anywhere from 1 - 4 weeks, depending on the complexity of the design and the number of revisions you require. The development phase requires an additional 1 - 6 weeks, depending on the number of pages, and the functionality required. Of course, these are general guidelines, and we will make every effort to meet the timeframe you have in mind.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse3c" aria-expanded="false" aria-controls="collapse3c">
                                                                        <span> </span>
                                                                        <p class="red">How much does a website cost?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3c">
                                                                <div class="panel-body">
                                                                    <p>The cost of a website can vary depending on various factors, just like the cost of a house may vary. Though our website projects generally start in the $5,000 range for basic business sites and range upward depending on your unique needs.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse4c" aria-expanded="false" aria-controls="collapse4c">
                                                                        <span> </span>
                                                                        <p class="red">How does the payment process work?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4c">
                                                                <div class="panel-body">
                                                                    <p>The project starts with a 50% deposit. After design sign off and before we move into programming, we collect 25%. Once we have completed and fulfilled our scope, the final 25% is collected and your website is then scheduled for launch.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse5c" aria-expanded="false" aria-controls="collapse5c">
                                                                        <span> </span>
                                                                        <p class="red">What kind of businesses do you work with?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5c">
                                                                <div class="panel-body">
                                                                    <p>We work with a broad range of company types [small start-ups, large corporations, nonprofits, B2B, B2C and more across many business industries [technology, food, apparel, health + beauty, camps, travel, finance, arts, fair trade, and more.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse6c" aria-expanded="false" aria-controls="collapse6c">
                                                                        <span> </span>
                                                                        <p class="red">How long does it take to build a website?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6c">
                                                                <div class="panel-body">
                                                                    <p>Our standard websites take approximately 10 - 30 days to create. Our E-commerce (online store) websites take approximately 160 days to create. This time will vary from project to project.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading7c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse7c" aria-expanded="false" aria-controls="collapse7c">
                                                                        <span> </span>
                                                                        <p class="red">Who will I work with during the project?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse7c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7c">
                                                                <div class="panel-body">
                                                                    <p>This is a great question to ask and you should ask it of any web design and development firm you are considering. Many firms will farm out work to freelancers or interns. Some firms give little or no access to client’s who want to speak directly with their designers or developers. Our clients work directly with our tight knit crew of 5. Who that will be depends on your project needs and what stage of the process you are in during the project.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse8c" aria-expanded="false" aria-controls="collapse8c">
                                                                        <span> </span>
                                                                        <p class="red">Can you help me write content for my website?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse8c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8c">
                                                                <div class="panel-body">
                                                                    <p>Yes. We include copy writing and editing in all of our proposals. We also build your sitemap and help structure the foundation of your website in the planning stages.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse9c" aria-expanded="false" aria-controls="collapse9c">
                                                                        <span> </span>
                                                                        <p class="red">Can you help me update my existing website that another web firm built?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9c">
                                                                <div class="panel-body">
                                                                    <p>No. We only support websites that we have fully built. We have this policy so that we can 100% stand behind everything that we craft. There are issues of liability of multiple programmers touching the same code over time.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10c">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse10c" aria-expanded="false" aria-controls="collapse10c">
                                                                        <span> </span>
                                                                        <p class="red">Do I own my website?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse10c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10c">
                                                                <div class="panel-body">
                                                                    <p>YES! Everything that we build will be 100% owned by you. Most of our clients stay with us for the life of the website. Our team is just irresistible that way. But if for any reason you decide you want to take your site to another hosting service and get another company to service the site, we will happily assist you in making the transition as effortlessly and efficiently as possible. The website is yours after all, and we want you to take it wherever you go.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                            </div>

                        </div> <!-- end  col-md-12 -->

                      </div> <!-- end row -->

                    </div><!-- end  Section 3 -->





                    <!-- Start  Section 4 -->
                    <div role="tabpanel" class="row tab-pane" id="Section4">

                       <div class="row">  <!-- start row --> 

                         <div class="col-xs-12 col-md-12"> <!-- Start  col-md-12 -->

                                             
                            <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                                                    
                            <h3 class="text-center">Company services</h3>


                                <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse1e" aria-expanded="false" aria-controls="collapse1e">
                                                                        <span> </span>
                                                                        <p class="red">What does confirmation statement mean?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse1e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1e">
                                                                <div class="panel-body">
                                                                    <p>Every company must confirm the information that companies house hold about it is correct by delivering a confirmation statement, From 30 June 2016 the confirmation statement replaces the annual return.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse2e" aria-expanded="false" aria-controls="collapse2e">
                                                                        <span> </span>
                                                                        <p class="red">What does confirmation date mean?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse2e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2e">
                                                                <div class="panel-body">
                                                                    <p>The date at which the company is confirming all the required information is up to date. This must be no later than 12 months after the last confirmation date.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse3e" aria-expanded="false" aria-controls="collapse3e">
                                                                        <span> </span>
                                                                        <p class="red">Is It criminal offence not file your confirmation statement?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3e">
                                                                <div class="panel-body">
                                                                    <p>Yes, It’s a criminal offence to not file your confirmation statement within 14 days of the end of the review period. If you don’t do this,Companies House may prosecute the company and its officers.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse4e" aria-expanded="false" aria-controls="collapse4e">
                                                                        <span> </span>
                                                                        <p class="red">What does VAT stand for?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4e">
                                                                <div class="panel-body">
                                                                    <p>Value Add Tax </p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse5e" aria-expanded="false" aria-controls="collapse5e">
                                                                        <span> </span>
                                                                        <p class="red">When do I register for VAT?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5e">
                                                                <div class="panel-body">
                                                                    <p>You must register for VAT with HMRC if your business’ VAT taxable turnover is more than £85,000</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse6e" aria-expanded="false" aria-controls="collapse6e">
                                                                        <span> </span>
                                                                        <p class="red">What do I get when i register VAT?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6e">
                                                                <div class="panel-body">
                                                                    <p>You will get VAT registration certificate. This confirms:</p>
                                                                    <p>1,your VAT number</p>
                                                                    <p>2,when to submit your first VAT Return and payment</p>
                                                                    <p>3,your ‘effective date of registration’ - this is the date you went over the threshold, or the date you asked to register if it was voluntary
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading7e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse7e" aria-expanded="false" aria-controls="collapse7e">
                                                                        <span> </span>
                                                                        <p class="red">can I register VAT voluntarily?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse7e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7e">
                                                                <div class="panel-body">
                                                                    <p>You can register voluntarily if your turnover is less than £85,000, unless everything you sell is exempt. You’ll have certain responsibilities if you register for VAT.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse8e" aria-expanded="false" aria-controls="collapse8e">
                                                                        <span> </span>
                                                                        <p class="red">What is my responsibilities after i registered?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse8e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8e">
                                                                <div class="panel-body">
                                                                    <p>From the effective date of registration you must:</p>
                                                                    <p>1,charge the right amount of VAT</p>
                                                                    <p>2,pay any VAT due to HMRC</p>
                                                                    <p>3,submit VAT Returns</p>
                                                                    <p>4,keep VAT records and a VAT account</p>
                                                                    <p>5,You can also reclaim the VAT you’ve paid on certain purchases made before you registered.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse9e" aria-expanded="false" aria-controls="collapse9e">
                                                                        <span> </span>
                                                                        <p class="red">How often do I submit a VAT Return?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9e">
                                                                <div class="panel-body">
                                                                    <p>You usually submit a VAT Return to HM Revenue and Customs (HMRC) every 3 months. This period of time is known as your ‘accounting period.’</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10e">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse10e" aria-expanded="false" aria-controls="collapse10e">
                                                                        <span> </span>
                                                                        <p class="red">How many types of VAT's rate?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse10e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10e">
                                                                <div class="panel-body">
                                                                    <p>There are 3 differents rate of VAT.</p> 
                                                                    <p>1,Standard rate</p>
                                                                    <p>2,Reduced rate</p>
                                                                    <p>3,Zero rate </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        

                            </div>

                        </div> <!-- end  col-md-12 -->

                      </div> <!-- end row -->

                    </div> <!-- end  Section 4 -->


   


                </div> <!-- end tab-content -->

             </div> <!-- end   col-md-8 -->

            </div> <!-- end class="tab" role="tabpanel"-->

        </div><!-- end  col-md-12  -->


    </div><!-- end  row -->

</div>  <!-- end  container -->

<!-- end  of help -->



<?php include 'footer.php'; ?>

