<?php 
require("validator.php");
?>

<?php include 'header.php'; ?>

<div class="container"> <!-- start container -->

     <h2 class="text-center red">Contact us</h2>

    <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid green !important; " />

  <div>  <!-- start row -->

       <div class="col-xs-12 col-md-6">  <!-- start form row -->

           <h5 class="green">The dedicated Almost Cost Free team is based in London, wherever you are, you can reach us. we pride ourselves on our outstanding support and going above and beyond. we won’t rest until you’ve accomplished your goals.</h5>

           <?php if(!empty($user_msg)){ unset($_POST); }?>

            <form  action="contactus.php" method="post">
               <div class="row">
                  <div class="col-sm-6 form-group">
                     <input class="form-control 
                        <?php if (!empty($error_array["firstname"])){ echo "is-invalid"; }else{ if(!empty($_POST["firstname"])){ echo "is-valid"; } }?>" id="firstname" name="firstname" value="<?php if(!empty($_POST['firstname'])){echo $_POST['firstname']; }?>" placeholder="First name" type="text" required>
                     <?php if (!empty($error_array["firstname"])){ ?>
                     <small id="passwordHelp" class="text-danger">
                     <?php echo $error_array["firstname"]; ?>
                     </small> 
                     <?php } ?>
                  </div>
                  <div class="col-sm-6 form-group">
                     <input class="form-control <?php 
                        if (!empty($error_array["surname"])){ echo "is-invalid"; }else{ 
                            if(!empty($_POST["surname"])){ echo "is-valid"; } }?>" id="surname" name="surname" placeholder="Surname" value="<?php if(!empty($_POST['surname'])){echo $_POST['surname']; }?>" type="text" required>
                     <?php if (!empty($error_array["surname"])){ ?>
                     <small id="passwordHelp" class="text-danger">
                     <?php echo $error_array["surname"]; ?>
                     </small> 
                     <?php } ?>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 form-group">
                     <input class="form-control <?php 
                        if (!empty($error_array["email"])){ echo "is-invalid"; }else{ 
                            if(!empty($_POST["email"])){ echo "is-valid"; } }?>" id="email" name="email" placeholder="Your email e,g youremail@yahoo.com" value="<?php if(!empty($_POST['email'])){echo $_POST['email']; }?>" type="email" required>
                     <?php if (!empty($error_array["email"])){ ?>
                     <small id="passwordHelp" class="text-danger">
                     <?php echo $error_array["email"]; ?>
                     </small> 
                     <?php } ?>
                  </div>
                  <div class="col-sm-6 form-group">
                     <input class="form-control <?php 
                        if (!empty($error_array["phone"])){ echo "is-invalid"; }else{ 
                            if(!empty($_POST["phone"])){ echo "is-valid"; } }?>" id="phone" name="phone"  placeholder="Phone number e,g 078########" value="<?php if(!empty($_POST['phone'])){echo $_POST['phone']; }?>" type="tel" pattern="[0-9]*" required>
                     <?php if (!empty($error_array["phone"])){ ?>
                     <small id="passwordHelp" class="text-danger">
                     <?php echo $error_array["phone"]; ?>
                     </small> 
                     <?php } ?>
                  </div>
               </div>
               <div class="row">
                  <div class=" col-sm-12 form-group">
                     <select name="state" id="state" class="form-control selectpicker <?php 
                        if (!empty($error_array["state"])){ echo "is-invalid"; }else{ 
                            if(!empty($_POST["state"])){ echo "is-valid"; } }?>">
                        <option value="">Please Select</option>
                        <option value="Companyformation" <?php if(!empty($_POST["state"])){ echo "selected"; }?>>Company formation</option>
                        <option value="Webdesign" <?php if(!empty($_POST["state"])){ echo "selected";  }?>>Web design</option>
                        <option value="Confirmationstatement" <?php if(!empty($_POST["state"])){ echo "selected"; }?>>Confirmation statement</option>
                        <option value="VATregistration" <?php if(!empty($_POST["state"])){ echo "selected";  }?>>VAT registration</option>
                        <option value="Companydissolved" <?php if(!empty($_POST["state"])){ echo "selected";  }?>>Company dissolved</option>
                        <option value="Generalenquire" <?php if(!empty($_POST["state"])){ echo "selected";  }?>>General enquire</option>

                     </select>
                     <?php if (!empty($error_array["state"])){ ?>
                     <small id="passwordHelp" class="text-danger">
                     <?php echo "Kindly select a state." ?>
                     </small> 
                     <?php } ?>
                  </div>
               </div>
               <textarea class="form-control <?php 
                  if (!empty($error_array["comments"])){ echo "is-invalid"; }else{ 
                      if(!empty($_POST["comments"])){ echo "is-valid"; } }?>" id="comments" name="comments" placeholder="Please enter your massage here. e,g call me" rows="5" value="<?php if(!empty($_POST['comments'])){echo $_POST['comments']; }?>" required><?php if(!empty($_POST['comments'])){echo $_POST['comments']; }?></textarea>
               <?php if (!empty($error_array["comments"])){ ?>
               <small id="passwordHelp" class="text-danger">
               <?php echo $error_array["comments"]; ?>
               </small> 
               <?php } ?>
               <br>
               <div class="row">
                  <div class="col-sm-12 form-group">
                     <button class="btn btn-lg btn-block" type="submit">Submit</button>
                  </div>
                  
               </div>
               <?php if(!empty($user_msg)){  ?>
                  <div class="alert alert-success">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Success!</strong> <?php echo $user_msg; ?>
                  </div>
            <?php } ?>

            </form>

      </div> <!-- end form row -->




      <div class="col-xs-12 col-md-6"> <!-- start row -->

          <h3 class="red">Opening hours</h3>
          <p><span class="green">Monday :</span> 9:30 am - 5:30 pm</p>
          <p><span class="green">Tuesday :</span> 9:30 am - 5:30 pm</p>
          <p><span class="green">Wednesday :</span> 9:30 am - 5:30 pm</p>
          <p><span class="green">Thursday :</span> 9:30 am - 5:30 pm</p>
          <p><span class="green">Friday :</span> 9:30 am - 5:30pm</p>
          <p><span class="green">Saturday &amp; Sunday :</span> Closed</p>
          <p><span class="green">Public Holidays : </span> Closed</p>
          <p><span class="green">Email :</span> contactus@almostcostfree.com</p>

      </div> <!-- end row -->


</div> <!-- end row -->




<div class="row"> <!-- start map row -->
         
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.169495793624!2d-0.2148535987531806!3d51.51010632953557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760fde08e30485%3A0x3e854cddb991f0bd!2sLondon+W11+4AL!5e0!3m2!1sen!2suk!4v1510609673885" class="col-xs-12" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>

</div> <!-- end map row -->


<br>

</div> <!-- end container-->



<?php include 'footer.php'; ?>






