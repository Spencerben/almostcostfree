<?php include 'header.php'; ?>


<!-- Start website design-->

<div id="webdesign" class="container">

    <h2 class="text-center red">Web design</h2>

    <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid green !important;" />

    <br>
 
    <h5 class="text-center red">We believe that your business need a website to show your customers what product or service you offer and also need to take payment online through your website,</h5>


    <!-- start row -->
    <div class="row">



            <div class="col-md-4">


                    <ul class="nav">
                    

                            <li class="active"><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"  data-toggle="tab"  href="#accountantandlawyer">Accountant &amp; Lawyer</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#restaurantandpizzashop">Restaurant &amp; Pizza Shop</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#beautysalonandbarber">Beauty Salon &amp; Barber</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#electronicshopandelectrician">Electronic Shop &amp; Electrician </a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#constructionandplumber">Construction &amp; Plumber</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#estateagentandtravelagent">Estate Agent &amp; Travel Agent</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#minicabanddeliveryservice">Mini Cab &amp; Delivery Service</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#photographerandinteriordesign">Photographer &amp; Interior design</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#bakeryandfactory">Bakery &amp; Factory</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#dentistandpharmacy">Dentist &amp; Pharmacy</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#educationandtraining">Education &amp; Training</a></li>
                            <li><a class="col-lg-12 col-md-6 col-sm-6 col-xs-6"   data-toggle="tab" href="#ecommerceandorganization">E-commerce &amp; Organization</a></li>

                    </ul>

            </div>


            <div class="col-md-8">



                    <div class="tab-content">



                            <div id="accountantandlawyer" class="tab-pane fade in active" >
                                   

                                                <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                    <img src="images/p1.jpg" class="img-responsive"/>
                                                    <i class="shine_effect"></i>
                                                     
                                                </span>
                                          
                                               <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                    <img src="images/p2.jpg"  class="img-responsive"/>
                                                    <i class="shine_effect"></i>
                                                     
                                              </span>


                                                 <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                    <img src="images/p3.jpg" class="img-responsive"/>
                                                    <i class="shine_effect"></i>
                                                      
                                                </span>
                                           
                                               <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                    <img src="images/p4.jpg"  class="img-responsive"/>
                                                    <i class="shine_effect"></i>
                                                    
                                                </span>


                            </div>

                                     


                            <div id="restaurantandpizzashop" class="tab-pane fade">


                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p5.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p6.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p7.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p8.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>


                            </div>



                            <div id="beautysalonandbarber" class="tab-pane fade">

                                    
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p9.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                          
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p10.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                          </span>

                                             <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p11.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                           
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p12.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>


                            </div>



                            <div id="electronicshopandelectrician" class="tab-pane fade">


                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p13.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p14.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p15.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p16.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                              

                            </div>



                            <div id="constructionandplumber" class="tab-pane fade">
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p17.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                          
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p18.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                          </span>

                                             <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p19.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                           
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p20.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>


                            </div>


                            <div id="estateagentandtravelagent" class="tab-pane fade">

                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p21.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p22.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p23.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p24.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                               
                                            </span>


                            </div>


                            <div id="minicabanddeliveryservice" class="tab-pane fade">

                                          <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p25.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                          
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p26.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                               
                                          </span>

                                             <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p27.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                           
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p28.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>


                            </div>



                            <div id="photographerandinteriordesign" class="tab-pane fade">

                              
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p29.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p30.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p31.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p32.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                               

                            </div>



                            <div id="bakeryandfactory" class="tab-pane fade">

                              
                                             <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p33.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                               
                                            </span>
                                          
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p34.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                          </span>

                                             <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p35.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                           
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p36.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                               

                            </div>



                            <div id="dentistandpharmacy" class="tab-pane fade">

                                       
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p37.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p38.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p39.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                  
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p40.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>


                            </div>



                            <div id="educationandtraining" class="tab-pane fade">
                              
                                           <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p41.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p42.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p43.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p44.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>

                              

                            </div>


                            <div  id="ecommerceandorganization" class="tab-pane fade">


                                          <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p45.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>

                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p46.jpg"  class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                           
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p47.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                 
                                            </span>
                                            
                                            <span class="col-md-6 col-sm-6 col-xs-6 shine_me">
                                                <img src="images/p48.jpg" class="img-responsive"/>
                                                <i class="shine_effect"></i>
                                                
                                            </span>


                            </div>

                         
                     <nav aria-label="Page navigation" class="text-center">
                         <ul id="alt-style-pagination" class="pagination simple-pagination">
                            <li>
                              <a href="#" aria-label="Previous" class="prevBtn">
                                <span aria-hidden="true">&laquo;</span>
                              </a>
                            </li>
                            <li class="active"><a href="#accountantandlawyer" data-toggle="tab">1</a></li>
                            <li><a href="#restaurantandpizzashop" data-toggle="tab">2</a></li>
                            <li><a href="#beautysalonandbarber" data-toggle="tab">3</a></li>
                            <li><a href="#electronicshopandelectrician" data-toggle="tab">4 </a></li>
                            <li><a href="#constructionandplumber" data-toggle="tab">5</a></li>
                            <li><a href="#estateagentandtravelagent" data-toggle="tab">6</a></li>
                            <li><a href="#minicabanddeliveryservice" data-toggle="tab">7</a></li>
                            <li><a href="#photographerandinteriordesign" data-toggle="tab">8</a></li>
                            <li><a href="#bakeryandfactory" data-toggle="tab">9</a></li>
                            <li><a href="#dentistandpharmacy" data-toggle="tab">10</a></li>
                            <li><a href="#educationandtraining" data-toggle="tab">11</a></li>
                            <li><a href="#ecommerceandorganization" data-toggle="tab">12</a></li>    
                            <li>
                              <a href="#" aria-label="Next" class="nextBtn">
                                <span aria-hidden="true">&raquo;</span>
                              </a>
                            </li>
                       </ul>
                    </nav>

                    <script>
                     jQuery('.nextBtn').click(function(e){
                         e.preventDefault();
                      jQuery('.pagination > .active').next('li').find('a').trigger('click');
                    });

                      jQuery('.prevBtn').click(function(e){
                          e.preventDefault();
                      jQuery('.pagination > .active').prev('li').find('a').trigger('click');
                    });
                    </script>

                            
                   </div>


            </div>




    </div><!-- end row -->


<br>


</div> <!-- end container -->





 <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid #89b416 !important; " />



<!-- start price compare of web design-->

<div class="container"> <!-- start container--> 

    <h4 class="red text-center">We are announcing perfect web design package for you</h4>

    <br>

    <table class="fluid">

        <thead>

            <tr class="table_top_row">

                    <th>&nbsp;</th>
                    <th>&nbsp;</th>

                    <th class="bg_yellow top">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th >

                    <th class="bg_yellow top">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>

                    <th class="bg_yellow top"> 
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>

                    <th class="bg_red top">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>

                    <th class="bg_yellow top"> 
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>
            </tr>

        </thead>

            <tr class="price">

                    <td  class="gb_white"> </td>
                    <td  class="gb_white"> </td>
                    <td  class="bg_green"> £349</td>
                    <td  class="bg_green"> £449</td>
                    <td  class="bg_green"> £549</td>
                    <td  class="bg_red white"> £749</td>
                    <td  class="bg_green"> £999</td>

            </tr>

            <tr>

                <td  class="gb_white"> </td>
                <td  class="gb_white"> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage1payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage2payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage3payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage4payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage5payform.php">Buy Now</a> </td>
                
            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> domain name .co.uk /.com for 1 Year</td>

                <td>
                    <a tabindex="1" class="btn" role="button"  title="domain name" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="A domain name like www.yourdomainname.com is a website address that people will use to find you on the internet."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> unlimited disk space</td>
                <td>
                    <a tabindex="2" class="btn" role="button" title="unlimited disk space" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="The amount of space allocated to store your website files, databases and mail for any email accounts you have set up."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> Content Management System</td>
                <td>
                    <a tabindex="3" class="btn" role="button"  title="wordpress website" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="WordPress is the world’s most popular Content Management System (CMS) allowing people to build websites and manage content with no limits."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> fully responsive / mobile friendly websites</td>
                <td>
                    <a tabindex="4" class="btn" role="button"  title="Responsive design" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="All our websites are made with a responsive design, this means you'll be able to view your website on laptops,mobiles,tablets and desktops without any problems."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> homepage Banner</td>
                <td>
                    <a tabindex="5" class="btn" role="button"  title="Homepage Banner" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create homepage banner form on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> Unlimited FTP</td>
                <td>
                    <a tabindex="6" class="btn" role="button"  title="Unlimited FTP" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="File Transfer Protocol (FTP) is the standard network protocol used for the transfer of computer files between a client and server on a computer network."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> Contact Form</td>
                <td>
                    <a tabindex="7" class="btn" role="button"  title="Contact Form" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create contact us form on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>
                <td class="left_align"> <span class="red">Free</span> Social Media Integration</td>
                <td> 
                    <a tabindex="8" class="btn" role="button"  title="Social Media Integration" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="It is the act of spreading your brand across the popular social media platforms, and being active on all them."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> email address</td>
                <td>
                    <a tabindex="9" class="btn" role="button"  title="email address" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Create private, password protected email addresses (name@mydomain.com) allowing you to send and receive using any mail client, such as Microsoft Outlook, Mac Mail or our WebMail interface. Email access can also be quickly setup and synced to your mobile device."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> UK web hosting for 1 Year then Upgrading</td>
                <td>
                    <a tabindex="10" class="btn" role="button"  title="UK web hosting" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="A web hosting service is a type of Internet hosting service. It allows people and companies to make their website available on the World Wide Web."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> Google Maps Integration</td>
                <td>
                    <a tabindex="12" class="btn" role="button" title="Google Maps Integration" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create google map on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> UK-Based Support</td>
                <td>
                    <a tabindex="13" class="btn" role="button"  title="UK-Based Support" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we pride ourselves on our outstanding support and going above and beyond. we won’t rest until you’ve accomplished your goals."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> unlimited bandwidth</td>
                <td>
                    <a tabindex="14" class="btn" role="button"  title="bandwidth" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="bandwidth describes the level of traffic and amount of data that can transfer between your site, users, and the Internet."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> photo gallary on your website</td>
                <td>
                    <a tabindex="15" class="btn" role="button"  title="photo gallary" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create photo gallary on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> £75 Google Adwords Voucher</td>
                <td>
                    <a tabindex="16" class="btn" role="button"  title="Google Adwords Voucher" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Attract new customers and drive more targeted traffic to your website with £75 Google AdWords vouchers"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> 99.9% Service Uptime</td>
                <td>
                    <a tabindex="17" class="btn" role="button"  title="99.9% Service Uptime" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Uptime is the opposite of downtime, Uptime is a computer industry term for the time during which a computer is operational"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> 30 Day Money Back Guarantee</td>
                <td>
                    <a tabindex="18" class="btn" role="button"  title="Money Back Guarantee" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="if a buyer is not satisfied with our product or service, a refund will be made*"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                </a>
                </td>
               <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

             <tr>

                <td class="left_align"> <span class="red">Free</span> youtube video Integration</td>
                <td>
                    <a tabindex="19" class="btn" role="button"  title="youtube video Integration" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create youtube video on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> database integration</td>
                <td>
                    <a tabindex="20" class="btn" role="button" title="database integration" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we create dynamic website e,g booking and login system,"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>   
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> shopping cart</td>
                <td>
                    <a tabindex="21" class="btn" role="button"  title="shopping cart" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="a shopping cart is a piece of e-commerce software on a web server that allows visitors to an Internet site to select items for eventual purchase,"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> content writing</td>
                <td>
                    <a tabindex="22" class="btn" role="button"  title="content writing" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="A website content writer specializes in providing relevant content for websites"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr class="price bottom">

                    <td  class="gb_white"> </td>
                    <td  class="gb_white"> </td>
                    <td  class="bg_green"> £349</td>
                    <td  class="bg_green"> £449</td>
                    <td  class="bg_green"> £549</td>
                    <td  class="bg_red white"> £749</td>
                    <td  class="bg_green"> £999</td>

            </tr>

            <tr>

                <td  class="gb_white"> </td>
                <td  class="gb_white"> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage1payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage2payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage3payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage4payform.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="webdesignpackage5payform.php">Buy Now</a> </td>
                    
            </tr>
            
                        
        </table>
    <br>

</div> <!-- end container--> 

<!-- end price compare of web design -->


<?php include 'footer.php'; ?>



