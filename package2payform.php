<?php include 'header.php'; ?>

<!-- start of container -->
<div class="container">

    <div class="row">

        <div class="col-xs-12 col-md-6">

          <form role="form" id="personalinfo_form" method="post" action="#"> 

              <table>
                   <tr style="background-color:#e81512;">
          
                      <th class="text-center">Payment</th>
                      <th class="text-center">form</th>
                      

                   </tr>

                   <tr>
                     
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>

                   </tr> 

                   <!-- start Presonal information -->

                  <tr style="background-color:#89b416;">
                    
                    <td>Personal</td>
                    <td>information</td>
                  </tr>

             

                   <tr>   

                      <!-- Name -->
                      <td>
                           <label for="firstname" class="control-label">First Name</label>
                           <p id="errfirstname" class="errblock"></p>
                           <input type="text" class="form-control" id="firstname" name="firstname" required />
                       </td>                                            
                     
                       <!-- Address -->
                       <td>
                            <label for="lastname" class="control-label">Last Name</label>
                            <p id="errlastname" class="errblock"></p>
                            <input type="text" class="form-control" id="lastname" name="lastname" required/>
                       </td>

                   </tr> 


                    <tr>   

                      <!-- Name -->
                      <td>
                           <label for="email" class="control-label">Email</label>
                           <p id="erremail" class="errblock"></p>
                           <input type="text" class="form-control" id="email" name="email" required/>
                       </td>                                            
                     
                       <!-- Address -->
                       <td>
                            <label for="phone" class="control-label">Phone</label>
                            <p id="errphone" class="errblock"></p>
                            <input type="text" class="form-control" id="phone" name="phone" required/>
                       </td>

                   </tr> 

                    <tr>   

                      <!-- Name -->
                      <td>
                           <label for="housename" class="control-label">House Name</label>
                           <p id="errhousename" class="errblock"></p>
                           <input type="text" class="form-control" id="housename" name="housename" required/>
                       </td>                                            
                     
                       <!-- Address -->
                       <td>
                            <label for="streetname" class="control-label">Street Name</label>
                            <p id="errstreetname" class="errblock"></p>
                            <input type="text" class="form-control" id="streetname" name="streetname" required/>
                       </td>

                   </tr>

                    <tr>   

                      <!-- Name -->
                      <td>
                            <label for="town" class="control-label">Town</label>
                            <p id="errtown" class="errblock"></p>
                            <input type="text" class="form-control" id="town" name="town" required/>
                       </td>                                            
                     
                       <!-- Address -->
                       <td>
                            <label for="county" class="control-label">County</label>
                            <input type="text" class="form-control" id="county" name="county" />
                       </td>

                   </tr>


                    <tr>   

                      <!-- Name -->
                      <td>
                           <label for="country" class="control-label">Country</label>
                           <p id="errcountry" class="errblock"></p>
                           <input type="text" class="form-control" id="country" name="country" required/>
                       </td>                                            
                     
                       <!-- Address -->
                       <td>
                            <label for="postcode" class="control-label">Post Code</label>
                            <p id="errpostcode" class="errblock"></p>
                            <input type="text" class="form-control" id="postcode" name="postcode" required/>
                       </td>

                   </tr> 

                   <!-- end Personal information-->


                      <!-- start Payment infromation -->


                    <tr style="background-color:#89b416;">
                      
                      <td>Payment</td>
                      <td>information</td>
                      
                    </tr>

                    <tr>   

                      <!-- Name -->
                      <td>
                           <label for="nameoncard" class="control-label">Name on card</label>
                           <p id="errnameoncard" class="errblock"></p>
                           <input type="text" class="form-control" id="nameoncard" name="nameoncard" required/>
                       </td>                                            
                     
                       <!-- Card number -->
                       <td>
                             <label for="cardnumber" class="control-label">Card number</label>
                             <p id="errcardnumber" class="errblock"></p>
                             <input type="text" class="form-control" id="cardnumber" name="cardnumber" required/>
                       </td>

                   </tr> 


                    <tr>   

                      <!-- Card type -->
                      <td>
                           <label for="cardtype" class="control-label">Card type</label>
                           <p id="errcardtype" class="errblock"></p>

                           <select class="custom-select" id="cardtype" name="cardtype">
                                  <option value="0">- Select card type -</option>
                                  <option value="1">American Card</option>
                                  <option value="2">MasterCard</option>
                                  <option value="3">Visa</option>
                                  <option value="4">Other</option>
                            </select>
                       </td>                                            
                     
                       <!-- Select card type -->
                       <td>
                            <label for="ccv" class="control-label">CCV <i class="fa fa-question-circle help-icon" data-toggle="tooltip" data-placement="top" title="This number is printed on your cards in the signature area of the back of the card."></i></label>
                          
                           <p id="errccv" class="errblock"></p>
                          
                          <input type="text" class="form-control" id="ccv" maxlength="3" name="ccv" required/>

                       </td>

                   </tr> 

                    <!-- Expriation and CCV -->
                

                    <tr>

                      <td>
                            <label for="Expriation-type" class="control-label">Expriation month:</label>
                            <p id="errmonth" class="errblock"></p>
                                              <select class="custom-select" id="month" name="month">
                                                <option value="0">Month</option>
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>                                                
                                              </select>

                       </td>

                      

                       <td>
                         
                              <label for="Expriation-type" class="control-label">Expriation year:</label>
                              <p id="erryear" class="errblock"></p>
                                                <select class="custom-select" id="year" name="year">
                                                <option value="0">Year</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                                <option value="2027">2027</option>
                                                <option value="2028">2028</option>
                                                </select>
                       </td>

                   </tr>

                  

                  <!-- end Payment information-->


              

              </table>                         
               </form>                               
                
              <article class="col-xs-12">
                                                         
                            <!-- previous form data --> 
                            <input type="hidden" name="itemextra" id="itemextra" value="<?php if(isset($_POST['itemextra'])){ print_r($_POST['itemextra']);}; ?>">
                            <input type="hidden" name="packagename" id="packagename" value="package2">
                            <input type="hidden" name="totalamount" id="totalamount" value="<?php if(isset($_POST['totalamount'])){ print_r($_POST['totalamount']);}; ?>">
                            <input type="hidden" name="host" id="host" value="">
                                       


                            <!-- Continue button -->              
                            <h3 class="text-center"><a class="bg_red  buybtn" href="#">Save &amp; Continue</a> </h3>

                    
                                                                
                </article>
                <!-- end Checkout form -->


                <div class="clearfix"> </div>


          </div>  

          <!-- end of first column -->






         <!-- start of second column -->

          <?php /*session_start();*/ $_SESSION['redirecturl'] = 'registeredofficeform.php'; ?>
              
          <!-- Your Order package -->
          <div class="col-xs-12 col-md-6" id="orderditems">
                    
                <table id="orderditems">

                  <?php if(isset($_POST['itemdiv'])){
                          print_r(base64_decode($_POST['itemdiv']));
                        }; ?>

                </table>
                 
          </div> 

          <!-- end of second column -->




    </div> <!-- end of row -->


    <br><br>



</div> <!-- end of container -->



<?php include 'footer.php'; ?>


