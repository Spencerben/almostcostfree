<?php include 'header.php'; ?>



<!-- start Company Dissolved  -->

<div class="container">

          
<div class="row">

        <div class="col-xs-12">
    
            <h3 class="red text-center">Company Dissolved</h3>
                       
            <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid green !important; " />
               
            <h3 class="text-center"><a class="bg_red  buybtn" href="companydissolvedpayform.php">Order Now &nbsp; &pound; 19</a> </h3> 
                     
            <h3 class="green">You can close down your limited company by getting it ‘struck off’ the Companies Register, but only if it:</h3>

            <ul>
                <li>hasn’t traded or sold off any stock in the last 3 months</li>
                <li>hasn’t changed names in the last 3 months</li>
                <li>isn’t threatened with liquidation</li>
                <li>has no agreements with creditors, eg a Company Voluntary Arrangement (CVA)</li>
            </ul>

            <h5>If your company doesn’t meet these conditions, you’ll have to voluntarily liquidate your company instead.</h5>


            <h5>When you apply to ‘strike off’ your company, you have certain responsibilities to close down your business properly.</h5>

            <h3 class="green">Close down your company</h3>

            <h4>Before applying to strike off your limited company, you must close it down legally. This involves:</h4>
            <ul>
                <li>announcing your plans to interested parties and HM Revenue and Customs (HMRC)</li>
                <li>making sure your employees are treated according to the rules</li>
                <li>dealing with your business assets and accounts</li>
            </ul>

            <h3 class="green">Who you must tell</h3>
            <h4>Fill in an application to strike off and send a copy within 7 days to anyone who could be affected. This includes:</h4>

            <h4>members (usually the shareholders)</h4>
            <ul>
                <li>creditors</li>
                <li>employees</li>
                <li>managers or trustees of any employee pension fund</li>
                <li>any directors who didn’t sign the application form</li>
            </ul>

            <h4>If you don’t follow the rules on who you must tell, you can face a fine and possible prosecution.</h4>

            <h3 class="green">Employees</h3>
            <h4>If your company employs staff, you must:</h4>
            <ul>
                <li>follow the rules if you make staff redundant</li>
                <li>pay their final wages or salary</li>
                <li>PAYE and National Insurance (NI)</li>
                <li>You’ll need to tell HMRC that your company has stopped employing people.</li>
            </ul>


            <h3 class="green">PAYE and National Insurance (NI)</h3>
            <ul>
            <li>You’ll need to tell HMRC that your company has stopped employing people.</li>
            </ul>

            <h3 class="green">Business assets</h3>

            <h5>You should make sure that any business assets are shared among the shareholders before the company is struck off.</h5>

            <h5>Anything that’s left will go to the Crown - you’ll have to restore the company to get anything back.</h5>



            <h3 class="green">Final accounts</h3>
            <h5>You must send final statutory accounts and a Company Tax Return to HMRC.</h5>

            <h5>You don’t have to file final accounts with Companies House.</h5>
            <h5>Prepare your final accounts and company tax return.</h5>
            <h5>File your accounts and company tax return, stating that these are the final trading accounts and that the company will soon be dissolved.</h5>
            <h5>Pay all Corporation Tax and any other outstanding tax liabilities.</h5>
            <h5>If you’ve made a loss in your final year of trading, you might be able to offset the tax against profits from previous years - this is known as ‘terminal loss relief’. You can claim this on your final tax return.</h5>


            <h3 class="green">Capital Gains Tax on personal profits</h3>
            <h5>If you take assets out of the company before it’s struck off, you might have to pay Capital Gains Tax on the amount.</h5>

            <h5>You might be able to get tax relief on this through Entrepreneurs’ Relief.</h5>

            <h5>You will work this out on your personal Self Assessment tax return.</h5>

            <h5>If the amount is worth more than £25,000, it will be treated as income and you’ll have to pay Income Tax on it.</h5>


            <h3 class="green">Keeping records</h3>
            <h5>You must keep business documents for 7 years after the company is struck off, eg bank statements, invoices and receipts.</h5>

            <h5>If the company employed people, you must keep copies of its employers’ liability insurance policy and schedule for 40 years from the date the company was dissolved.</h5>



            <h3 class="green">Apply to strike off</h3>

            <h5>To apply to strike off your limited company, you must send Companies House form DS01.</h5>

            <h5>The form must be signed by a majority of the company’s directors.</h5>

            <h5>You should deal with any of the assets of the company before applying, eg close any bank accounts and transfer any domain names.</h5>

            <h5>When your company is dissolved, all the remaining assets will pass to the Crown (including any bank balances).</h5>

            <h5>It costs £10 to strike off a company. You can’t pay using a cheque from an account that belongs to the company you’re striking off.</h5>


            <h4>It is an offence to make a dishonest application - you can face a fine and possible prosecution.</h4>

            <h3 class="green">What happens next</h3>

            <h5>You’ll get a letter from Companies House to let you know if you’ve filled in the form correctly. If you have, your request for the company to be struck off will be published as a notice in your local Gazette.</h5>

            <h5>If nobody objects, the company will be struck off the register once the 2 months mentioned in the notice has passed.</h5>

            <h5>A second notice will be published in the Gazette - this will mean the company won’t legally exist anymore (it will have been ‘dissolved’).</h5>


            <h3 class="green">Withdraw your application</h3>

            <h5>You must withdraw your application if your company is no longer eligible to be struck off, eg it is trading or has become insolvent.</h5>

            <h5>You can withdraw your application if you change your mind. You can only do this if your company is still on the Companies Register.</h5>

            <h5>Only one director needs to sign the withdrawal form.</h5>

      </div>


</div> <!-- end row -->
        
      
<br>   

   
     <h3 class="text-center"><a class="bg_red  buybtn" href="companydissolvedpayform.php">Order Now &nbsp; &pound; 19</a> </h3> 


</div> <!-- end container-->



<!-- end Company Dissolved  -->






<?php include 'footer.php'; ?>





