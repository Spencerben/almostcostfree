<?php include 'header.php'; ?>

<!-- start Privacy and Cookies -->


<!-- start Privacy -->
<div class="container">

	<div class="row col-xs-12">


			<h2 class="red">Privacy on Almost Cost Free Ltd</h2>

			<h3 class="green">Is shopping online with Almost Cost Free Ltd safe?</h3>
			<p>Payment information you send us is SSL-encrypted, so outside parties can’t intercept, retrieve or use it</p>

			<p>At Almost Cost Free Ltd, we care about security and make every effort to ensure our transaction process is safe and that your personal information is secure. </p>

			<p>We can assure you that every order you make on our website is safe. We use strict security precautions to make our website safe, including the use of a Secure Socket Layer (SSL) server. Any information you enter while you're using the secure server is encrypted before being transmitted, so it is virtually impossible for an outside party to access or intercept your information.</p>

			<p>You can check that you are shopping in a secure environment by looking for either a locked padlock icon or an image of a key in the bar at the bottom of your screen. Additionally, your web address should start with https: - the ‘s’ indicating it is a secure page. This should appear on any page where you are entering personal information, such as your payment card details. </p>

			<p>The latest versions of the most popular browsers, Internet Explorer, Chrome, Safari and Firefox support this secure connection. If for any reason this option has been disabled on your browser, you will not be able to transact or login until you reset these options. Find this option in your browser settings under security.</p>
			 
			<h3 class="green">How do you protect my credit card details and personal information?</h3>
			<p>Our website is verified by Norton Secure, powered by Verisign to protect your personal data. Your payment methods are also covered through MasterCard Secure Code or Verified by Visa. </p>

			<p>For your additional security and to protect your personal information we request that you enter your password each time you wish to view a page containing your personal details. </p>

			<p>You can enrol your card in MasterCard SecureCode or Verified by Visa scheme if you are not already a part of them. You will be able to set a private code for online transactions, giving you added protection against the unauthorised use of your credit or debit card online.</p> 

			<p>The full details of our Privacy Policy are below in our official Privacy Policy. This policy will explain all about what information we collect from you, how we use that information and your rights around retrieving or removing any information we hold in relation to you, as well as our complete guide to the use of cookies on our website.</p>
			   




			<h3 class="green">Our Privacy Policy</h3>
			    <p>Last updated: 07.06.2016 </p>
			<h3>1,Who we are</h3> 
			<h3>2,What information we collect and how</h3> 
			<h3>3,What we do with your information</h3> 
			<h3>4,Your rights</h3>
			<h3>5,Other websites</h3>


			<h3 class="green">Who we are</h3>

			<p>In this Privacy Policy references to ‘we’ and ‘us’ are to Almost Cost Free Ltd,, company number 09668223, registered office: 1 Romlilly House ,Kenley Walk, London, W11 4AL.</p>



			<h3 class="green">What information we collect and how</h3>

			<p>The information we collect via this website may include:</p>
			<p>(1) Any personal details you type in and submit, such as name, address, email address, etc. We only collect information that we require for completion of orders and account set up. We never store payment details on the websites. </p>

			<p>(2) Data which allows us to recognise you, your preferences (including items added to your basket) and how you use this website. This saves you from re-entering information when you return to the site. This data is collected by cookies from your navigation around the site. A complete guide to our cookies policy and how to control cookies on your computer can be found on our complete guide to cookies here . </p>

			<p>(3) Your IP address (this is your computer’s individual identification number) which is automatically logged by our web server. This is used to note your interest in our website and your location (e.g. county/city) for our site analytics. </p>

			<p>(4) Your preferences and use of email updates, recorded by emails we send you (if you select to receive email updates on products and offers).</p>


			<h3 class="green">What we do with your information</h3>

			<p>Any personal information we collect from you will be used in accordance with the Data Protection Act 1998 and other applicable laws. The details we collect will be used: </p>

			<p>(1) to process your order, to process any application for finance or a mobile phone contract etc, to maintain guarantee records and to provide pre and after-sales service (we may pass your details to another organisation to supply/deliver products or services you have purchased and/or to provide pre and after-sales service). We make use of the TouchCommerce system to provide our web chat functionality. Data provided via web chat may be stored on servers based in the United States under a "safe harbour agreement". This means that the data will be managed to similar standards to those required under UK Data Protection legislation; and </p>

			<p>(2) to carry out security checks (this may involve passing your details to our Identity Verification partner, currently 192.com, who will check details we give them against public and private databases - this helps to protect you and us from fraudulent transactions); and </p>

			<p>(3) to comply with legal requirements</p>
			<p>We may need to pass the information we collect to other companies within our group for administrative purposes. We may use third parties to carry out certain activities, such as processing and sorting data, monitoring how customers use our site and issuing our emails for us. </p>

			<p>We would also like to inform you of various promotions, goods and services that may be of interest to you. These would come from any of our chains, from our affiliates, or from carefully selected third parties. You may be contacted by post, email, telephone, SMS or such other means as we regard as appropriate, including new technology. If you wish to receive these communications, please tick the ‘Yes’ box when entering your personal details. You may unsubscribe at a later date, if you wish - ‘Your rights’ section below.</p>



			<h3 class="green">How can I see a copy of the information you hold about me?</h3>

			<h3>The Data Protection Act (DPA) gives you the right to see personal information we hold about you. If you want copies of this information, please send us a written request with the following:</h3>
			<p>
			Your full name and address</p> <p>
			Your account number</p> <p>
			A cheque for £10 (admin charge) made payable to Dixons Carphone</p><p>
			Details of any specific information you require</p> <p>
			One proof of identity - we can accept a photocopy of either your passport or your driver's licence</p><p>
			One proof of identity of address - we can accept a photocopy of a recent credit or debit card statement, a utility bill showing the same name and address as on your account.</p>


			<h3 class="red">Do not send original documentation as we cannot guarantee the safe receipt or return of these.</h3>

			<h3 class="green">Please send your written request, cheque and proof of identity and address to:</h3>
			<p>Dixons Carphone</p>
			<p>Data Protection Officer</p>
			<p>PO Box 375</p>
			<p>Southampton</p>
			<p>SO30 2PU</p>
			 
			<p>We recommend you send your request by Recorded Delivery but this is not compulsory. Please note that we do not take online payments.</p>


			<h3 class="green">What happens next?</h3>
			<p>Once we receive your request, along with your cheque and proof of identity, we'll get back to you within 40 days.</p>


			
     </div>


</div>

<!-- end Privacy-->




<!-- start Cookies -->
<div class="container">

		<div class="row col-xs-12">

	
					<h3 class="red">Computing Cookies</h3>

					<p>When you are browsing websites on the internet, many of these websites store tiny text files called cookies on your computer, to help track your use of the site and to personalise your journey around the website. These cookies can be stored so that if you return to a website, that website server can call the information from the cookies stored on your computer to tailor your experience of the site. </p>

					<p>Almost Cost Free Ltd is no different, we use cookies to help you shop our website more effectively and to place online orders - we do not store personally identifiable information in our cookie data . We also use some carefully selected 3rd party suppliers to enhance your online experience and they will place cookies on your computer for use on our website too. </p>

					<p>Cookies are perfectly safe to be stored on your computer and almost all web browsers have cookie storing enabled as default. However, all browsers have the option of disabling cookies being stored on your computer if you wish to do this.</p> 

					<p>Please be aware that disabling cookies on your browser will reduce your ability to shop online with www.asmostcostfree.com. We use cookies to process products in your basket and orders. Disabling your cookies will mean you cannot purchase through our website. This would also be a common experience of reduced functionality across many websites. </p>

					<h3 class="green" >To learn about how change the cookie settings for your browser on our Cookies & Your Computer page. </h3>

					<h3 class="green">Cookies & Your Computer</h3>

					<p>Cookies should be enabled by default on your browser, so if you have not changed your settings you should expect to be accepting cookies from websites. </p>

					<p>Cookies being disabled will limit your experience of PCWorld.co.uk and many other websites online. Remember, cookies are not harmful to your computer and our cookies on PCWorld.co.uk do not contain any personally identifiable information. If you wish to change your cookie settings we have provided a guide below for you. Please remember , if you do disable your cookies, you can allow cookies from certain approved websites, by adding the website homepage to your exceptions list. </p>

					<p>First, find out what your default web browser is on your machine, or the browser you use most often to access the internet. To work out what browser you are using, simply open the internet as usual, and then follow the steps below: </p>

					<h3 class="green" >PC Users </h3>

					<p>On the tool bar at the top of your browser click 'Help' and choose the 'About' option from the drop down. This will tell you the browser type and version. </p>

					<h3 class="green">Mac Users </h3>

					<p>Open the internet as usual and once open, click on the Apple icon in the top left corner and choose the 'About' option. This will again tell you the browser type and version.</p>

					<p>Once you know the browser you are using you can find the cookie settings using the guide below relevant for your browser:</p>




					<h3 class="green">Checking your settings in Internet Explorer 8 and earlier versions</h3>

					<p>1,Locate the 'Tools' tab on the menu areas of your browser. Click on this. </p>
					<p>2,There is an option called ‘Internet Options’ on the list, select this option. </p>
					<p>3,This should open a pop up box with lots of tabbed options. Select the ‘Privacy’ tab from the list.</p> 
					<p>4,Internet Explorer has a high, medium, low auto-adjust system for internet content handling.</p> <p>You can configure your own settings by clicking the advanced tab and ticking the ‘Override automatic cookie handling’</p>
					<p>5,Settings above Medium will disable cookies, Medium or below will enable cookies</p>


					<h3 class="green">Checking your settings in Mozilla Firefox</h3>

					<p>1,Locate the 'Tools' tab on the menu areas of your browser. Click on this. </p>
					<p>2,From the list of options on the drop down menu, click on ‘Options’</p>
					<p>3,There should be a pop up window appear with tabs along the top. Find the ‘Privacy’ tab and select this.</p>
					<p>4,You should now have some options for ‘Tracking’ and ‘History’. Under the ‘History’ drop down menu, select ‘Use customer settings for history’ if you wish to adjust your cookie settings.</p>

					<h3 class="green">Checking your settings in Google Chrome</h3>

					<p>1,Look for the spanner icon in Chrome  and click on this</p>
					<p>2,From this menu, select settings…</p>
					<p>3,Within settings you will see tabs on the left hand side. Select the tab called ‘Under the Hood’</p>
					<p>4,When this page loads you will see a tab at the top called ‘Content Settings’ – select this</p>
					<p>5,This brings up your content controls including cookie permissions – select the option you feel  most comfortable with</p>
								

					<h3 class="green">Checking your settings in Safari</h3>

				    <p>1,Locate the Cog icon at the top of your browser  and click on this.</p>
					<p>2,From the list of options select ‘Preferences’</p>
					<p>3,This should open a pop up window with several tabs along the top. From this list select the ‘Privacy’ option. </p>
				    <p>4,This will open the tab page that allows you to control the cookie settings on your browser.</p>

					<h3 class="green">Checking your settings in Opera</h3>

					<p>1,Click the Opera file icon in the top left hand corner of your browser. </p>
					<p>2,From the drop down list of options, highlight ‘Settings’ and then scroll across and select ‘Preferences’.</p>
					<p>3,This should open a pop up window with tabs along the top. Choose the ‘Advanced’ tab. </p>
					<p>4,This shows you the advanced options. On the left hand side there is an option called ‘Cookies’ – click on this option. </p>
					<p>5,This presents the cookie options for your use on this browser.</p>

					<p>You can visit the following website for an in-depth guide to cookies and how to control, delete and understand them. They even have a great recipe on there for cookies of the edible variety – yum! http://www.aboutcookies.org/</p>
							 
					<p>Recent legislation from the European Union has meant that websites must provide clear information about their use of cookies to their customers, something we at Almost Cost Free Ltd fully support. We want to ensure that you, as our valued customer, are fully aware of the use of cookies on our website, and we are proud of our reputation as a transparent and honest services provider that you can trust.</p>

				

		</div>

</div>


<!-- end Cookies -->


<!-- end Privacy and Cookies -->




<?php include 'footer.php'; ?>



