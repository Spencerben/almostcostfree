<!-- start footer -->


<!--start world wide contact-->

<div class="container-fluid bg_yellow">

  <div class="container">

  <div class="row">


  <div class="col-xs-12 col-sm-4 col-md-4">


  <h3 class="red text-center">Approved by</h3>

  <div class="col-xs-4 text-center">

  <h5 class="green">HMRC</h5>

  <img class="img-rounded img-responsive col-sm-12 flagborder" alt="HMRC" src="images/approved/hmrc.png">


</div>


<div class="col-xs-4 text-center">

<h5 class="green">ICO</h5>

<img class="img-rounded img-responsive col-sm-12 flagborder" alt="ICO" src="images/approved/ico.png">

</div>


<div class="col-xs-4 text-center">

<h5 class="green">CH</h5>

<img class="img-rounded img-responsive col-sm-12 flagborder" alt="Company House" src="images/approved/companyhouse.png">


</div>


<br>

</div>


<div class="col-xs-12 col-sm-4 col-md-4">


<h3 class="red text-center">World Wide</h3>


<div class="col-xs-4 text-center">

<h5 class="green">UK</h5>
<img class="img-rounded img-responsive col-sm-12 flagborder" alt="UK flag" src="images/flag/uk.png">
</div>


<div class="col-xs-4 text-center">

<h5 class="green">Kenya</h5>

<img class="img-rounded img-responsive col-sm-12 flagborder" alt="Kenya flag" src="images/flag/kenya.png">

</div>


<div class="col-xs-4 text-center">

<h5 class="green">Australia</h5>
<img class="img-rounded img-responsive col-sm-12 flagborder" alt="Australia flag" src="images/flag/australia.png">
</div>



<br>


</div>


<div class="col-xs-12 col-sm-4 col-md-4">

<h3 class="red text-center">Secured by</h3>

<div class="col-xs-4 text-center">

<h5 class="green">Barclays</h5>

<img class="img-rounded img-responsive col-sm-12 flagborder" alt="Barclays bank" src="images/approved/barclays.png">


</div>


<div class="col-xs-4 text-center">

<h5 class="green">SSL</h5>

<img class="img-rounded img-responsive col-sm-12 flagborder" alt="LLoyds Bank" src="images/approved/ssl.png">

</div>


<div class="col-xs-4 text-center">

<h5 class="green">PayPal</h5>

<img class="img-rounded img-responsive col-sm-12 flagborder" alt="PayPal" src="images/approved/paypal.png">


</div>


<br>

</div>


</div>


<hr class="col-md-12 col-xs-12"  style="border-top: 1px solid #89b416 !important; " />
</div>


<!--end world wide contact-->








<div class="container">



  <footer>

    <section>






      <div class="col-sm-12 col-md-12"> <!--start class="col-md-12"-->



        <div class="col-xs-12 col-sm-4 col-md-4">


          <h5 class="red">Company Services</h5>

          <ul>
            <li><a href="companyformation.php">Company formation</a></li>
            <li><a href="confirmationstatement.php">Confirmation statement</a></li>
            <li><a href="vatregistration.php">VAT registration</a></li>
            <li><a href="companydissolved.php">Company dissolved</a></li>

          </ul>


        </div>

        <div class="col-xs-12 col-sm-4 col-md-4">


          <h5 class="red">Webdesign Services</h5>
          <ul>
           
            <li><a href="webdesign.php">Webdesign</a></li>


          </ul>


        </div>



        <div class="col-xs-12 col-sm-4 col-md-4">



          <h5 class="red">Company Information</h5>
          <ul>
            <li><a href="aboutus.php" target="_blank">About us</a></li>
            <li><a href="help.php" target="_blank">Help / FAQ</a></li>
            <li><a href="termsandconditions.php" target="_blank">Terms &amp; conditions</a></li>
            <li><a href="privacyandcookies.php" target="_blank">Privacy &amp; cookies</a></li>
          </ul>


        </div>




      </div> <!--end class="col-md-12"-->




      <div class="col-xs-12 col-sm-12 col-md-12">


        <ul  class="text-center">

          <p>Company reg No: 09668223,<span class="hidden-sm hidden-md hidden-lg"><br></span> VAT reg No: 265661679,<span class="hidden-sm hidden-md hidden-lg"><br></span> Reg office: 1 Romilly House, <span class="hidden-sm hidden-md hidden-lg"><br></span>Kenley Walk, W11 4AL, London</p>



        </ul>

      </div>


      <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid #89b416 !important; " />

      <div class="col-md-12"> <!--start class="col-md-12"-->


        <div class="col-xs-12 col-sm-5  col-md-5 text-center">


       

           
         <p>© 2017 by Almost Cost <span class="red"> Free</span> Ltd</p> 



        </div> <!--end class="col-md-12"-->




        <div class="col-xs-12 col-sm-5 col-md-5">

          <ul  class="text-center">

            <li>
              <a href="#" class="fa fa-facebook" target="_blank" ></a>
              <a href="#" class="fa fa-twitter" target="_blank"></a>
              <a href="#" class="fa fa-google" target="_blank"></a>
              <a href="#" class="fa fa-linkedin" target="_blank"></a>
            </li>

          </ul>


        </div>




        <div class="col-xs-12 col-sm-2 col-md-2">

          <ul class="text-right">

            <h2>
              <a href="#scroll" class="red">

                <i class="fa fa-angle-double-up" style="font-size:48px;color:#e81512;"></i>

              </a>

            </h2>


          </ul>
        </div>




      </div> <!--end class="col-md-12"-->



    </section>

  </footer>

</div><!--/.container-->




</div> <!-- end container-fluid -->


<!-- end footer -->


<script type="text/javascript" src="js/custom.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>




<script type="text/javascript">
  
/*----------------------------------------*/
/*  start pagination for web design menu  */
/*----------------------------------------*/


jQuery('body').on('click', function () {
  if (jQuery('#myCarousel').find('input').eq(0).is(':focus') || jQuery('#myCarousel').find('input').eq(1).is(':focus')) {
    jQuery('#myCarousel').carousel('pause');
  } else {
    jQuery('#myCarousel').carousel('cycle');
  }
});

jQuery('.nextBtn').click(function(e){
  e.preventDefault();
  jQuery('.pagination > .active').next('li').find('a').trigger('click');
});

jQuery('.prevBtn').click(function(e){
  e.preventDefault();
  jQuery('.pagination > .active').prev('li').find('a').trigger('click');
});

jQuery(function() {
  jQuery('.simple-pagination li').slice(3,11).hide();
  jQuery('.simple-pagination li:eq(3)').after('<li class="pagination-space"><a href="#" data-toggle="tab">...</a></li>');
});

jQuery('.simple-pagination li').on('click', function() {
  if((!jQuery(this).find('a').hasClass('prevBtn')) && (!jQuery(this).find('a').hasClass('nextBtn'))) {
    jQuery('li.pagination-space').remove();
    jQuery('.simple-pagination li').slice(2,12).hide();
    jQuery(this).show();
    jQuery(this).nextAll('li:lt(1)').show();
    jQuery(this).prevAll('li:lt(1)').show();
    if(jQuery(this).find('a').html()!='2') jQuery(this).prevAll('li:eq(1)').after('<li class="pagination-space"><a href="#" data-toggle="tab">...</a></li>');
    if(jQuery(this).find('a').html()!='11') jQuery(this).nextAll('li:eq(1)').before('<li class="pagination-space"><a href="#" data-toggle="tab">...</a></li>');
  }
});


/*----------------------------------------*/
/*  end pagination for web design menu  */
/*----------------------------------------*/





/*-----------------------------------*/
/*  start for popover                */
/*-----------------------------------*/

 jQuery(document).ready(function(){
      
      jQuery('[data-toggle="popover"]').popover();
    
    });
  

/*-----------------------------------*/
/*  end for popover            */
/*-----------------------------------*/



</script>




</body>

</html>
