<?php include 'header.php'; ?>

<!-- start bannerslide-->

<div class="container-fluid">
 
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000" data-keyboard="true">

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
       <div class="item active">
             <div class="carousel-caption">
                    <div class="row bannertext">     
                           <form action="example2.php" method="post" class="form-inline text-center">
                                  <h2 class="red">Please register your new company name</h2>
                                            <br>

                                <div class="input-group input-group-lg">
                                        <input type="text" name="company_name" id="company_name" maxlength="60" class="form-control" size="50" placeholder="e,g yourcompanyname Limited or Ltd" style="text-transform:uppercase" required>

                                        <div class="input-group-btn">
                                            <button type="submit" class="btn" ><span class="glyphicon glyphicon-search"></span><span class="hidden-xs hidden-sm"> Search</span></button>
                                        </div>
                                </div>

                          </form>

                   </div>
 
             </div>  

       </div>
    </div>

 </div>

</div> <!-- end container-fluid -->

<!-- end bannerslide -->




<!-- start price compare-->

<div class="container"> <!-- start container--> 

 <h1 class="page_title">Please select package below</h1>

    <table class="fluid">

         <thead>

            <tr class="table_top_row" >

                    <th>&nbsp;</th>
                    <th>&nbsp;</th>

                    <th class="bg_yellow top">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th >

                    <th class="bg_yellow top">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>

                    <th class="bg_yellow top"> 
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>

                    <th class="bg_red top">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>

                    <th class="bg_yellow top"> 
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </th>
            </tr>

        </thead>

        <tbody>

            <tr class="price">

                    <td  class="gb_white"> </td>
                    <td  class="gb_white"> </td>
                    <td  class="bg_green"> £15</td>
                    <td  class="bg_green"> £19</td>
                    <td  class="bg_green"> £49</td>
                    <td  class="bg_red white"> £349</td>
                    <td  class="bg_green"> £399</td>

            </tr>

            <tr>

                <td  class="gb_white"> </td>
                <td  class="gb_white"> </td>
                <td><a class="bg_red  buybtn" href="package1.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="package2.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="package3.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="package4.php">Buy Now</a> </td>
                <td><a class="bg_red  buybtn" href="package5.php">Buy Now</a> </td>
                
            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> register your new company with in 3 hours &amp; also <span class="red">includes £12 government fees</span></td>

                <td>
                    <a tabindex="1" class="btn" role="button"  title="included £12 government fees." data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can register your new company with in 3 hours and also included £12 government fees."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> email copy of certificate of incorporation</td>
                <td>
                    <a tabindex="2" class="btn" role="button" title="certificate of incorporation" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="is a legal document relating to the formation of a company or corporation. It is a license to form a corporation issued by companies house"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> email copy of memorandum and articles of association</td>
                <td>
                    <a tabindex="3" class="btn" role="button" title="memorandum and articles of association" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="is a legal statement signed by all initial shareholders agreeing to form the company and also written rules about running the company agreed by the shareholders, directors and the company secretary."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> email copy of share certificate</td>
                <td>
                    <a tabindex="4" class="btn" role="button" title="share certificate" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="is a legal document that certifies ownership of a specific number and type of of shares you own in a company"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> printed certificate of incorporation</td>
                <td>
                    <a tabindex="5" class="btn" role="button" title="certificate of incorporation" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="is a legal document relating to the formation of a company or corporation. It is a license to form a corporation issued by companies house"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> printed your company share certificate(s)</td>
                <td>
                    <a tabindex="6" class="btn" role="button" title="share certificate" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="is a legal document that certifies ownership of a specific number and type of of shares you own in a company"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>

                <td class="left_align"> <span class="red">Free</span> how to setup and run your business ebook</td>
                <td>
                    <a tabindex="7" class="btn" role="button" title="how to setup and run your business ebook" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we will give you free ebook that can help you how to setup and run your business."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>
            
            <tr>
                <td class="left_align"> <span class="red">Free</span> business bank account</td>
                <td> 
                    <a tabindex="8" class="btn" role="button" title="Business bank account" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can help you to open a Barclays business bank account with free banking for 12 months"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> domain name .co.uk /.com for 1 Year</td>
                <td>
                    <a tabindex="9" class="btn" role="button" title="domain name" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="A domain name like www.yourdomainname.com is a website address that people will use to find you on the internet."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> 48 professional fully responsive / mobile friendly websites</td>
                <td>
                    <a tabindex="10" class="btn" role="button" title="Responsive design" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="All our websites are made with a responsive design, this means you'll be able to view your website on laptops,mobiles,tablets and desktops without any problems."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> secured UK web hosting for 1 Year</td>
                <td>
                    <a tabindex="11" class="btn" role="button" title="UK web hosting" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="A web hosting service is a type of Internet hosting service. It allows people and companies to make their website available on the World Wide Web."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> unlimited bandwidth</td>
                <td>
                    <a tabindex="12" class="btn" role="button" title="bandwidth" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="bandwidth describes the level of traffic and amount of data that can transfer between your site, users, and the Internet."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> unlimited email address</td>
                <td>
                    <a tabindex="13" class="btn" role="button" title="email address" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Create private, password protected email addresses (name@mydomain.com) allowing you to send and receive using any mail client, such as Microsoft Outlook, Mac Mail or our WebMail interface. Email access can also be quickly setup and synced to your mobile device."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> unlimited disk space</td>
                <td>
                    <a tabindex="14" class="btn" role="button" title="unlimited disk space" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="The amount of space allocated to store your website files, databases and mail for any email accounts you have set up."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> Content Management System</td>
                <td>
                    <a tabindex="15" class="btn" role="button" title="wordpress website" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="WordPress is the world’s most popular Content Management System (CMS) allowing people to build websites and manage content with no limits."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> google map &amp; contact us form on your website</td>
                <td>
                    <a tabindex="16" class="btn" role="button" title="google map &amp; contact us form" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create google map and contact us form on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>



            <tr>

                <td class="left_align"> <span class="red">Free</span> youtube video &amp; photo gallary on your website</td>
                <td>
                    <a tabindex="17" class="btn" role="button" title="youtube video &amp; photo gallary " data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="we can create youtube video and photo gallary on your website with our proffesional web designers."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>



            <tr>

                <td class="left_align"> <span class="red">Free</span> £75 Google Adwords Voucher</td>
                <td>
                    <a tabindex="18" class="btn" role="button" title="Google Adwords Voucher" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Attract new customers and drive more targeted traffic to your website with £75 Google AdWords vouchers"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> VAT registration with HMRC</td>
                <td>
                    <a tabindex="19" class="btn" role="button" title="VAT registration" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="You must register for VAT if: your VAT taxable turnover is more than £85,000 (the 'threshold') in a 12 month period. you expect to go over the threshold in a single 30 day period."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>


            <tr>

                <td class="left_align"> <span class="red">Free</span> PAYE registration with HMRC</td>
                <td>
                    <a tabindex="20" class="btn" role="button" title="PAYE registration" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="PAYE is a tax collection procedure applied by HMRC to collect Income Tax and National Insurance contributions, If you are paying £113 or more a week to any of your employees, you must have to register for PAYE."><i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr>

                <td class="left_align"> <span class="red">Free</span> filing annual confirmation statement</td>
                <td>
                    <a tabindex="21" class="btn" role="button" title="confirmation statement" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Companies only have to file one confirmation statement every 12 months. However, you can choose to file a confirmation statement early, or more frequently than once a year."> <i class="fa fa-info-circle green" aria-hidden="true"></i>
                    </a>
                </td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>
                <td><i class="fa fa-times red" aria-hidden="true"></i></td>
                <td><i class="fa fa-check green" aria-hidden="true"></i></td>

            </tr>

            <tr class="price bottom">

                    <td  class="gb_white"> </td>
                    <td  class="gb_white"> </td>
                    <td  class="bg_green"> £15</td>
                    <td  class="bg_green"> £19</td>
                    <td  class="bg_green"> £49</td>
                    <td  class="bg_red white"> £349</td>
                    <td  class="bg_green"> £399</td>

            </tr>

            <tr>

                    <td></td>
                    <td></td>
                    <td><a class="bg_red  buybtn" href="package1.php">Buy Now</a> </td>
                    <td><a class="bg_red  buybtn" href="package2.php">Buy Now</a> </td>
                    <td><a class="bg_red  buybtn" href="package3.php">Buy Now</a> </td>
                    <td><a class="bg_red  buybtn" href="package4.php">Buy Now</a> </td>
                    <td><a class="bg_red  buybtn" href="package5.php">Buy Now</a> </td>
                    
            </tr>
            
      </tbody> 

   </table>
      <br>
</div> <!-- end container--> 

<!-- end price compare -->





 




              <!-- start website portofolio and Coming Soon -->

              <div class="container-fluid bg_yellow"><!-- start  container -->

                <div class="row">  


                   <!-- start website portofolio  -->
                   <div class="col-xs-12 col-sm-6 col-md-6"><!-- start  row -->

                        <h2 class="red text-center">Our portfolio</h2>

                        <h5 class="text-center">We design and develop websites from impressive designs to advanced functionality and smart usability according to the latest standards and technologies based on the demand of our clients.</h5> 
                      
                        

                                    <div id="myCarousel1" class="carousel slide" data-ride="carousel" data-interval="7000" >
                                            <!-- Indicators -->
                                          

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">


                                           
                                                      <div class="item active">
                                                        <img src="images/front1.jpg" alt="">
                                                      </div>

                                                      <div class="item">
                                                         <img src="images/front2.jpg" alt="">
                                                      </div>
                                                    
                                                      <div class="item">
                                                        <img src="images/front3.jpg" alt="">
                                                      </div>
                                                      
                                                      <div class="item">
                                                        <img src="images/front4.jpg" alt="">
                                                      </div>
                                                      
                                                      <div class="item">
                                                        <img src="images/front5.jpg" alt="">
                                                      </div>
                                    

                                           </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control red" href="#myCarousel1" data-slide="prev">
                                              <span class="glyphicon glyphicon-chevron-left"></span>
                                              <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control red" href="#myCarousel1" data-slide="next">
                                              <span class="glyphicon glyphicon-chevron-right"></span>
                                              <span class="sr-only">Next</span>
                                            </a>
                                    </div>


                            <h3 class="text-center"><span> <a href="webdesign.php"><button type="button" class="btn">See More Website</button></a></span></h3>
                   
                                <br>

                    </div>
                    <!-- end website portofolio  -->

                      

                     

                    <!-- start Coming Soon  -->
                    <div class="col-xs-12 col-sm-6 col-md-6 text-center"> <!-- start  row -->

                               
                             <h2 class="red text-center">Coming Soon</h2> 
                                       

                                 <h5 class="text-center">We are all in one including Accountancy Service Provider, we are planing to give a professional accounting services for our clients with excellent support and affordable price.</h5>  

                                        

                            


                             <div id="myCarousel2" class="carousel slide" data-ride="carousel" data-interval="7000" >
                                            <!-- Indicators -->
                                        

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">


                                           
                                                      <div class="item active">
                                                        <img src="images/comingsoon/vatreturn.png" alt="vatreturn">
                                                      </div>

                                                      <div class="item">
                                                         <img src="images/comingsoon/corporatetax.png" alt="corporatetax">
                                                      </div>
                                                    
                                                      <div class="item">
                                                        <img src="images/comingsoon/selfassessment.png" alt="selfassessment">
                                                      </div>
                                                      
                                                      <div class="item">
                                                        <img src="images/comingsoon/registeredoffice.png" alt="registeredoffice">
                                                      </div>
                                                      
                                                      <div class="item">
                                                        <img src="images/comingsoon/serviceaddress.png" alt="serviceaddress">
                                                      </div>
                                    

                                           </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control red" href="#myCarousel2" data-slide="prev">
                                              <span class="glyphicon glyphicon-chevron-left"></span>
                                              <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control red" href="#myCarousel2" data-slide="next">
                                              <span class="glyphicon glyphicon-chevron-right"></span>
                                              <span class="sr-only">Next</span>
                                            </a>
                         </div>




                         <h3 class="text-center"><button type="button" class="btn white">Need help give us a call on 078 2822 5551</button></h3>

                         <br>

                     </div>
                     <!-- end Coming Soon  -->


               </div><!-- end  row -->      
                      

         </div><!-- end  container -->

           

       <!-- end  website portofolio and Coming Soon -->






<!-- start Why Almost Cost Free?  -->


                <h3 class="text-center red">Why <span class="green">Almost Cost Free</span>?</h3>
                <p class="text-center">We are committed to your success</p>
<br>

         <div class="container"> <!-- start container-fluid -->

             
                
                
                
                

                 <div class="row">


                     <div class="col-xs-12 col-md-6">

                     
                            <div class="panel-group" id="accordion100" role="tablist" aria-multiselectable="true">
                           

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading1">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion100" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                                <span>1</span>
                                                <p class="red">We are a Company Service Provider <!-- approved by Company House --></p> 
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                        <div class="panel-body">
                                            
                                               <li>We are a company formation agent.</li>
                                               <li>Providing our services as nominee director or nominee company secretary.</li>
                                           
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading2">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion100" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                <span>2</span>
                                                <p class="red">Free Domain name registration</p>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                        <div class="panel-body">
                                            
                                            <li>We offer free Domain name registration if you buy one of our web design package.</li>
                                             
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading3">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion100" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                                <span>3</span>
                                                <p class="red">Get professional fully responsive / mobile friendly Website</p>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                        <div class="panel-body">

                                            
                                            <li>All our websites are made with a responsive design, this means you'll be able to view your website on laptops,mobiles,tablets and desktops without any problems,<a href="webdesign.php"> click here to see more design</a></li>
    
                                          

                                        </div>
                                    </div>
                                </div>



                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading4">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion100" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                                <span>4</span>
                                                <p class="red">Fast, affordable and secured UK based web hosting</p>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                        <div class="panel-body">
                                               
                                                    <li>Wordpress Hosting</li>
                                                    <li>Linux Hosting</li>
                                                                 
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading5">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion100" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                                <span>5</span>
                                                <p class="red">Expert on company services</p>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                        <div class="panel-body">
                                            

                                                     <li><a href="confirmationstatement.php">Confirmation statement</a></li>
                                                     <li><a href="vatregistration.php">VAT registration</a></li>
                                                     <li><a href="companydissolved.php">Company dissolved</a></li>
                                                              
                                             
                                        </div>
                                    </div>
                                </div>

                                

                       </div>

                  </div>
                


                  <div class="col-xs-12 col-md-6">

                         <div class="panel-group" id="accordion200" role="tablist" aria-multiselectable="true">



                                                       <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion200" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                                                        <span>6</span>
                                                                        <p class="red">We are supervised by HMRC.</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                                                <div class="panel-body">
                                                                 
                                                                    <li>We are supervised by HM Revenue &amp; Customs (HMRC) under the Money Laundering Regulations.</li>
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="heading7">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion200" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                                                            <span>7</span>
                                                                            <p class="red">24/7 server monitoring</p>
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                                                                    <div class="panel-body">
                                                                           
                                                                        <li>We monitor all our servers 24 hours a day, 7 days a week. This means you don’t need to worry if your website is online, because we’ll know if there are any issues before you do.</li>

                                                                            
                                                                    </div>
                                                                </div>

                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion200" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                                                        <span>8</span>
                                                                        <p class="red"> our payment online is managed by Barclays bank &amp; PayPal</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                                                                <div class="panel-body">
                                                                        <li>Barclays bank is one of the world's largest Bank,It has %0 brach all over the world and also faster, safer way to make an online payment.</li>

                                                                        <li>PayPal is one of the world's largest Internet payment companies, It is the faster, safer way to make an online payment.</li>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion200" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                                                        <span>9</span>
                                                                        <p class="red">Lowest price in the UK, No hidden fees or extras charges</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                                                                <div class="panel-body">
                                                                    
                                                                        <li>We always offer our customers as lowest price as our company name is (Almost Cost Free Ltd).</li>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion200" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                                                        <span>10</span>
                                                                        <p class="red">First class customer support</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                                                <div class="panel-body">
                                                                    
                                                                        <li>We specialize in customer satisfaction. Our world class customer support is available to you. We are committed to providing you with top notch support</li>
     
                                                                        <li>Let’s face it, problems really suck! But having a friendly support technician to talk to would make things so much easier right? We provide first class support to every one of our clients.</li>
                                                                   

                                                                </div>
                                                            </div>
                                                        </div>

                                                                      

                                  </div>

                              </div>


                         </div><!-- end  row -->

             
                   
           
                    <br>
           
             </div>  <!-- end container-fluid -->

             <!-- end Why Almost Cost Free? -->




       <!-- start  testimonial  -->
     
     <div class="container">

                <section id="carousel">                 
                    
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="red text-center">Here is what some of our customers said about us</h2>
                                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="8000">
                                  <!-- Carousel indicators -->
                                  <ol class="carousel-indicators">
                                    <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                                    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                                    <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                                  </ol>
                                  <!-- Carousel items -->
                                  <div class="carousel-inner">

                                     <div class="item active">
                                        <blockquote>
                                            <h3 class="text-center green"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></h3>

                                            <p> We are very pleased with the quality of your services,you have every thing what we need for our business (company formation, Domain registration,online marketing, web design &amp; hosting and also your staff are extremely helpful,we will recommend your company to our clients,friends and families, every body can afford it the price you charge for your services.</p>
                                        
                                            <h4 class="someone">Divid Williams, Senior Accountant</h4>
                                        </blockquote>
                                    </div>


                                    <div class="item">
                                        <blockquote>
                                            <h3 class="text-center green"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></h3>
                                            
                                            <p>AlmostCostFree has provided lots of services to us including web design and hosting. Our website was recommended as one of the best. Our long-term relationship with them and continued support has helped us excel. They are creative, on time with competitive pricing. We cannot say enough good about them and highly recommend their services to everyone.</p>

                                            <h4 class="someone">Janet Lewis, CEO</h4>

                                        </blockquote>
                                    </div>

                                    <div class="item">
                                        <blockquote>
                                            <h3 class="text-center green"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></h3>

                                            <p>Thank you so much for the wonderful work on the web design for our website. It turned out better than we had envisioned. It was a pleasure working with AlmostCostFree. The rapid responses to all of my questions and requests were much appreciated. I am looking forward to work with AlmostCostFree again in the near future.</p>

                                        <h4 class="someone">Andrew Davies, Project Director</h4>
                                        </blockquote>
                                    </div>

                                    <div class="item">
                                        <blockquote>
                                            <h3 class="text-center green"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></h3>

                                            <p>These guys are the best and I highly recommend them. Undule and the rest of the employees at AlmostCostFree have been nothing but professional from the very first day I talked to them. Frankly I can't say enough about them. They completed our project on time and under budget. Their skills are limitless!</p>
                                        
                                            <h4 class="someone">Kemal Sraj, Accounting Manager</h4>
                                        </blockquote>
                                    </div>

                                     <div class="item">
                                        <blockquote>
                                            <h3 class="text-center green"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></h3>

                                            <p>AlmostCostFree SEO has built me a great website and with the Google Maps listing I have seen an increase in traffic and business.You can find my listing on the top of Google.I wouldn’t hesitate to recommend AlmostCostFree SEO in the future, and if you are looking to promote your business online, then they are the perfect people for the job!</p>
                                        
                                            <h4 class="someone">Samantha Simon, Managing Director</h4>
                                        </blockquote>
                                    </div>

                                   

                                  </div>

                                   <!-- Left and right controls -->
                                            <a class="left carousel-control red" href="#fade-quote-carousel" data-slide="prev">
                                              <span class="glyphicon glyphicon-chevron-left"></span>
                                              <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control red" href="#fade-quote-carousel" data-slide="next">
                                              <span class="glyphicon glyphicon-chevron-right"></span>
                                              <span class="sr-only">Next</span>
                                            </a>

                                </div>


                            </div>                          
                        </div>
                    
                </section>


      </div>
      <!-- end  testimonial  -->

<br>





<?php include 'footer.php'; ?>




