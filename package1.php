<?php include 'header.php'; ?>


<?php
	//session_start();
	$link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $page = end($link_array); 
    $_SESSION['redirecturl'] = $page; //print_r($_SESSION); die();
    $base = str_replace($page, '', $link) ;
    
    if(!isset($_SESSION['CS_User']) || $_SESSION['CS_User'] == ''){
    	//header("Location: http://" . $_SERVER['SERVER_NAME'].$base."signupandlogin.php");
		//exit();
    } else {
    	$_SESSION['redirecturl'] = '';
    }
?>


<!-- start of container -->
<div class="container">

	<h3 class="text-center">You have aleardy selected your package and also you can add another service</h3>

	<div class="row">

		<div class="col-xs-12 col-md-6" >


		<table id="orderditems">


		     	<tr style="background-color:#e81512;">
		     		
			     	<th class="left_align">Your order Package</th>
			     	<th>Del</th>
			     	<th>Price</th>

		     	</tr>

		     	<tr>
		     		
			     	<td class="left_align">Package <i class="fa fa-star red" aria-hidden="true"></i>
			     	</td>
			     	<td></td>
			     	<td><span class="glyphicon glyphicon-gbp"></span> 15</td>

		     	</tr>


		     	<tr style="background-color:#89b416;">
		     		
			     	<th class="left_align">Selected Extras</th>
			     	<th>Del</th>
			     	<th>Price</th>

		     	</tr>

		     	<tr style=" display:none;" id="added_nominieedirector">
		     		
			     	<td class="left_align">Nominee company director</td>
			     	<td><span onclick="cartaction('remove','nominieedirector',199,1)" class="red glyphicon glyphicon-trash"></span></td>
			     	<td><span class="glyphicon glyphicon-gbp"></span> 199</td>

		     	</tr>

		     	<tr style=" display:none;" id="added_nominieesecretary">
		     		
			     	<td class="left_align">Nominee company secretary</td>
			     	<td><span onclick="cartaction('remove','nominieesecretary',99,2)" class="red glyphicon glyphicon-trash"></span></td>
			     	<td><span class="glyphicon glyphicon-gbp"></span> 99</td>

		     	</tr>


		     	<tr style=" display:none;" id="added_printeddoc">
		     		
			     	<td class="left_align">Printed documents Package</td>
					<td><span onclick="cartaction('remove','printeddoc',20,3)" class="red glyphicon glyphicon-trash"></span></td>
					<td><span class="glyphicon glyphicon-gbp"></span> 20</td>

		     	</tr>


		     	<tr style=" display:none;" id="added_domainname">
		     		
			     	<td class="left_align">domain name .co.uk /.com for 1 Year</td>
			     	<td><span onclick="cartaction('remove','domainname',10,4)" class="red glyphicon glyphicon-trash"></span></td>
			     	<td><span class="glyphicon glyphicon-gbp"></span> 10</td>

		     	</tr>

		     	<tr style=" display:none;" id="added_website">
		     		
			     	<td class="left_align">website Design</td>
					<td><span onclick="cartaction('remove','website',349,5)" class="red glyphicon glyphicon-trash"></span></td>
					<td><span class="glyphicon glyphicon-gbp"></span> 349</td>

		     	</tr>

		     	<tr style=" display:none;" id="added_webhosting">
		     		
			     	<td class="left_align">secured UK web hosting for 1 Year</td>
					<td><span onclick="cartaction('remove','webhosting',19,6)" class="red glyphicon glyphicon-trash"></span></td>
					<td><span class="glyphicon glyphicon-gbp"></span> 19</td>

		     	</tr>


		     	<tr style=" display:none;" id="added_vat">
		     		
			     	<td class="left_align">VAT registration with HMRC</td>
					<td><span onclick="cartaction('remove','vat',25,7)" class="red glyphicon glyphicon-trash"></span></td>
					<td><span class="glyphicon glyphicon-gbp"></span> 25</td>

		     	</tr>


		     	<tr style=" display:none;" id="added_paye">
		     		
			     	<td class="left_align">PAYE registration with HMRC</td>
					<td><span onclick="cartaction('remove','paye',25,8)" class="red glyphicon glyphicon-trash"></span></td>
					<td><span class="glyphicon glyphicon-gbp"></span> 25</td>

		     	</tr>


		     	<tr style=" display:none;" id="added_filingstatement">
		     		
			     	<td class="left_align">filing annual confirmation statement</td>
					<td><span onclick="cartaction('remove','filingstatement',25,9)" class="red glyphicon glyphicon-trash"></span></td>
					<td><span class="glyphicon glyphicon-gbp"></span> 25</td>

		     	</tr>
      		  

				 <tr class="red">
				 	
					 <td class="left_align">Net :</td>
					 <td></td>
					 <td><span class="glyphicon glyphicon-gbp"></span> <span id="nettotal">15</span></td>

				 </tr> 


				 <tr>
				 	
					 <td class="left_align">VAT :</td>
					 <td></td>
					 <td><span class="glyphicon glyphicon-gbp"></span> <span id="tax">3.00</span></td>

				 </tr> 



				 <tr class="red">
				 	
					 <td class="left_align">Total :</td>
					 <td></td>
					 <td><span class="glyphicon glyphicon-gbp"></span> <span id="total">18.00</span></td>

				 </tr> 
		
				
		</table>
				

				<br>

			    <div class="col-xs-12">
					
					<input type="checkbox" id="termsandcondtn" name="termsandcondtn" required> <span>terms and condtions</span>
					<p id="errtermsandcondtn" class="errblock"></p>

					<form method="post" id="saveitems" action="package1payform.php">
							<input type="hidden" name="itemdiv" id="itemdiv">
							<input type="hidden" name="itemextra" id="itemextra">
							<input type="hidden" name="packagename" id="packagename" value="package1">
							<input type="hidden" name="totalamount" id="totalamount">
				     </form>

				</div>

				<div class="col-xs-12 text-center">
						
						
		 		       <button type="button" class="btn buybtn" id='submitltd2star' onclick="savecart()">Save &amp; Continue</button>

		 		     <br>   <br>
				</div>

			
		

	</div> <!-- end of first  column -->








	<div class="col-xs-12 col-md-6">
			

		<table class="left_align">
				<tr style="background-color:#e81512;">
					
					<th class="left_align">Details of Additional Services</th>
					<th></th>
					<th></th>
					<th></th>

				</tr>

				<tr>
			     	<td>&nbsp;</td>
			     	<td>&nbsp;</td>
			     	<td>&nbsp;</td>
			     	<td>&nbsp;</td>

		     	</tr>

				<tr style="background-color:#89b416;">
					
					<th class="left_align">Company services Package</th>
					<th>Info</th>
					<th>Price</th>
					<th>Add</th>

				</tr>

				<tr id="add_nominieedirector">
					
					<td class="left_align">Nominee company director</td>
					<td>
						<a tabindex="1" class="btn" role="button" title="Nominee company director" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Nominee company director"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 199</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','nominieedirector',199,1);">Add</button></td>

				</tr>

				<tr id="add_nominieesecretary">
					
					<td class="left_align">Nominee company secretary</td>
					<td>
						<a tabindex="2" class="btn" role="button" title="Nominee company secretary" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Nominee company secretary"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 99</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','nominieesecretary',99,2);">Add</button></td>

				</tr>

				<tr style="background-color:#89b416;">
					
					<th class="left_align">Printed documents Package</th>
					<th>Info</th>
					<th>Price</th>
					<th>Add</th>

				</tr>

				<tr id="add_printeddoc">
					
					<td class="left_align">
						printed certificate of incorporation <br>
						printed your company share certificate(s) <br>
					    how to setup your business ebook <br>
					    Barclays business bank account</td>
					<td>
						<a tabindex="3" class="btn" role="button" title="Printed documents Package" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="Printed documents Package"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 20</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','printeddoc',20,3);">Add</button></td>

				</tr>

				<tr style="background-color:#89b416;">
					
					<th class="left_align">Website package</th>
					<th>Info</th>
					<th>Price</th>
					<th>Add</th>

				</tr>


				<tr id="add_domainname">
					
					<td class="left_align">domain name .co.uk /.com for 1 Year</td>
					<td>
						<a tabindex="3" class="btn" role="button" title="domain name .co.uk /.com for 1 Year" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="domain name .co.uk /.com for 1 Year"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 10</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','domainname',10,4);">Add</button></td>

				</tr>


				<tr id="add_website">
					
					<td class="left_align">website Design</td>
					<td>
						 <a tabindex="4" class="btn" role="button" title="website Design" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="website Design"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 349</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','website',349,5);">Add</button></td>

				</tr>


				<tr id="add_webhosting">
					
					<td class="left_align">secured UK web hosting for 1 Year</td>
					<td>
						<a tabindex="5" class="btn" role="button" title="secured UK web hosting for 1 Year" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="secured UK web hosting for 1 Year"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 19</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','webhosting',19,6);">Add</button></td>

				</tr>



				<tr style="background-color:#89b416;">
					
				   <th class="left_align">Accounting package</th>
				   <th>Info</th>
				   <th>Price</th>
				   <th>Add</th>

				</tr>


				<tr id="add_vat">
					 
					 <td class="left_align">VAT registration with HMRC</td>
					 <td>
					   	 <a tabindex="6" class="btn" role="button" title="VAT registration with HMRC" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="VAT registration with HMRC"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>
					 </td>
					 <td><span class="glyphicon glyphicon-gbp"></span> 25</td>
					 <td><button type="button" class="btn buybtn" onclick="cartaction('add','vat',25,7);">Add</button></td>

			    </tr>


			    <tr id="add_paye">

					  <td class="left_align">PAYE registration with HMRC</td>
					  <td>
					   	 <a tabindex="7" class="btn" role="button" title="PAYE registration with HMRC" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="PAYE registration with HMRC"><i class="fa fa-info-circle green" aria-hidden="true"></i>
                         </a>

					  </div>
					  <td><span class="glyphicon glyphicon-gbp"></span> 25</td>
					  <td><button type="button" class="btn buybtn" onclick="cartaction('add','paye',25,8);">Add</button></td>
			    </tr>

					

			   <tr id="add_filingstatement">

					<td class="left_align">filing annual confirmation statement</td>
					<td>
					   	 <a tabindex="8" class="btn" role="button" title="filing annual confirmation statement" data-trigger=“focus” data-toggle="popover" data-placement="right" data-content="filing annual confirmation statement"><i class="fa fa-info-circle green" aria-hidden="true"></i></a>

					</td>
					<td><span class="glyphicon glyphicon-gbp"></span> 25</td>
					<td><button type="button" class="btn buybtn" onclick="cartaction('add','filingstatement',25,9);">Add</button></td>
			   </tr>
       </table>

      </div> <!-- end  of second column -->

			

   </div> <!-- end of row -->



</div> <!-- end of container -->


<br><br>



<?php include 'footer.php'; ?>


