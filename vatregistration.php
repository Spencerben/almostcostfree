<?php include 'header.php'; ?>

<!-- start VAT Registration  -->

 <div class="container">

 <!-- start row-->        
 <div class="row">

             <h3 class="red text-center">VAT registration</h3>
             
             <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid green !important; " />

             <h3 class="text-center"><a class="bg_red  buybtn" href="vatregistrationpayform.php">Register Now &nbsp; &pound; 25</a> </h3>

            <!-- start first column-->
            <div class="col-xs-12 col-md-6"> 

                <h3 class="green text-center">FAQ for VAT registration</h3>
                 

                     <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                                   

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1g">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1g" aria-expanded="true" aria-controls="collapse1g">
                                                                        <span> </span>
                                                                        <p class="red">What does VAT stand for.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div id="collapse1g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1g">
                                                                <div class="panel-body">
                                                                    <p>Value Add Tax</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse2g" aria-expanded="false" aria-controls="collapse2g">
                                                                        <span> </span>
                                                                        <p class="red">When do I register for VAT.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse2g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2g">
                                                                <div class="panel-body">
                                                                    <p>You must register for VAT with HMRC if your business’ VAT taxable turnover is more than £85,000</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse3g" aria-expanded="false" aria-controls="collapse3g">
                                                                        <span> </span>
                                                                        <p class="red">What do I get when i register VAT.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3g">
                                                                <div class="panel-body">
                                                                    <p>You will get VAT registration certificate. This confirms:</p>

                                                                    <p>1,your VAT number</p>
                                                                    <p>2,when to submit your first VAT Return and payment</p>
                                                                    <p>3,your ‘effective date of registration’ - this is the date you went over the threshold, or the date you asked to register if it was voluntary
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse4g" aria-expanded="false" aria-controls="collapse4g">
                                                                        <span> </span>
                                                                        <p class="red">Can I register VAT voluntarily.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4g">
                                                                <div class="panel-body">
                                                                    <p>You can register voluntarily if your turnover is less than £85,000, unless everything you sell is exempt. You’ll have certain responsibilities if you register for VAT. </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse5g" aria-expanded="false" aria-controls="collapse5g">
                                                                        <span> </span>
                                                                        <p class="red">What is my responsibilities after i registered.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5g">
                                                                <div class="panel-body">
                                                                    <p>From the effective date of registration you must:</p>
                                                                    <p>1,charge the right amount of VAT</p>
                                                                    <p>2,pay any VAT due to HMRC</p>
                                                                    <p>3,submit VAT Returns</p>
                                                                    <p>4,keep VAT records and a VAT account</p>
                                                                    <p>5,You can also reclaim the VAT you’ve paid on certain purchases made before you registered.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse6g" aria-expanded="false" aria-controls="collapse6g">
                                                                        <span> </span>
                                                                        <p class="red">How often do I submit a VAT Return.? </p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6g">
                                                                <div class="panel-body">
                                                                    <p>You usually submit a VAT Return to HM Revenue and Customs (HMRC) every 3 months. This period of time is known as your ‘accounting period.’</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading7g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse7g" aria-expanded="false" aria-controls="collapse7g">
                                                                        <span> </span>
                                                                        <p class="red">What are VAT rates.?</p>
                                                                    </a>
                                                                </h4>
                                                           </div>
                                                         <div id="collapse7g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7g">
                                                                <div class="panel-body">
                                                                    <p>There are 3 different rates of VAT.</p> 
                                                                    <p>1,Standard rate</p>
                                                                    <p>2,Reduced rate</p>
                                                                    <p>3,Zero rate </p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                     <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse8g" aria-expanded="false" aria-controls="collapse8g">
                                                                        <span> </span>
                                                                        <p class="red">What does Standard rate mean.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>

                                                        <div id="collapse8g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8g">
                                                                <div class="panel-body">
                                                                    <p>Most goods and services are standard rate. You should charge this rate unless the goods or services are classed as reduced or zero-rated.</p>

                                                                    <p>This includes:<p>

                                                                    <p>1,Any goods below the distance selling threshold you supply to non-VAT registered EU customers - if you go over the threshold you’ll have to register for VAT in that country</p>
                                                                    <p>2,Most services you supply to an EU non-business customer - there are different rules for business customers</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse9g" aria-expanded="false" aria-controls="collapse9g">
                                                                        <span> </span>
                                                                        <p class="red">What does Reduced rate mean.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse9g">
                                                                <div class="panel-body">
                                                                    <p>When you charge this rate can depend on what the item is as well as the circumstances of the sale, for example:</p>

                                                                    <p>1,Children’s car seats and domestic fuel or power are always charged at 5%</p>

                                                                    <p>2,Mobility aids for older people are only charged at 5% if they’re for someone over 60 and the goods are installed in their home</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse10g" aria-expanded="false" aria-controls="collapse10g">
                                                                        <span> </span>
                                                                        <p class="red">What does Zero rate mean.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>

                                                            <div id="collapse10g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10g">
                                                                <div class="panel-body">
                                                                    <p>Zero-rated means that the goods are still VAT-taxable but the rate of VAT you must charge your customers is 0%. You still have to record them in your VAT accounts and report them on your VAT Return. Examples include:</p>
                                                                    <p>1,Books and newspapers</p>
                                                                    <p>2,Children’s clothes and shoes</p>
                                                                    <p>3,Motorcycle helmets</p>
                                                                    <p>4,Most goods you export to non-EU countries</p>
                                                                    <p>5,Goods you supply to a VAT registered EU business - you can   <a href="http://ec.europa.eu/taxation_customs/vies/vieshome.do?selectedLanguage=EN">check if the VAT number is valid</a></p>

                                                                </div>
                                                            </div>
                                                        </div>
                         
                        </div>
                                              


              

            </div> 

            <!-- end first column-->



             <!-- start second column-->

            <div class="col-xs-12 col-md-6">
           

                       <h3 class="green text-center">FAQ for VAT Registration</h3>
            
                         <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">

                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading11g">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse11g" aria-expanded="true" aria-controls="collapse11g">
                                                                        <span> </span>
                                                                    <p class="red">How long does it take time to get a VAT registration certificate.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse11g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11g">
                                                                <div class="panel-body">
                                                                    <p>You should get a VAT registration certificate within 14 working days, though it can take longer.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading12g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse12g" aria-expanded="false" aria-controls="collapse12g">
                                                                        <span> </span>
                                                                        <p class="red">How do I receive a VAT registration certificate.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse12g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12g">
                                                                <div class="panel-body">
                                                                    <p>It’s sent either:</p>
                                                                    <li>to your VAT online account</li>
                                                                    <li>by post - if an agent registers you or you can’t register online</li>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading13g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse13g" aria-expanded="false" aria-controls="collapse13g">
                                                                        <span> </span>
                                                                        <p class="red">Is there any threshold for busineess outside the UK to register VAT.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse13g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13g">
                                                                <div class="panel-body">
                                                                    <p>You will get VAT registration certificate. This confirms:</p>

                                                                    <p>There’s no threshold if neither you nor your business is based in the UK. You must register as soon as you supply any goods and services to the UK (or if you expect to in the next 30 days).</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading14g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse14g" aria-expanded="false" aria-controls="collapse14g">
                                                                        <span> </span>
                                                                        <p class="red">Is there any backdating claims for VAT.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse14g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14g">
                                                                <div class="panel-body">
                                                                    <p>There’s a time limit for backdating claims for VAT paid before registration. From your date of registration the time limit is:</p>
                                                                    <li>4 years for goods you still have, or that were used to make other goods you still have</li>
                                                                    <li>6 months for services</li>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading15g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse15g" aria-expanded="false" aria-controls="collapse15g">
                                                                        <span> </span>
                                                                        <p class="red">Can I cancel my VAT registration.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse15g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15g">
                                                                <div class="panel-body">
                                                                    <p>Yes, You can cancel your VAT registration.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading16g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse16g" aria-expanded="false" aria-controls="collapse16g">
                                                                        <span> </span>
                                                                        <p class="red">Can I transfer a VAT registration.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse16g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16g">
                                                                <div class="panel-body">
                                                                    <p>Yes,you can transfer a VAT registration from one business to another. For example, if you take over a company and want to keep using its VAT number.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading17g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse17g" aria-expanded="false" aria-controls="collapse17g">
                                                                        <span> </span>
                                                                        <p class="red">For how long do I keep VAT records.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse17g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17g">
                                                                <div class="panel-body">
                                                                    <li>You must keep VAT records for at least 6 years (or 10 years if you use the VAT MOSS service.</li>
                                                                    <li>You can keep VAT records on paper, electronically or as part of a software program (eg book-keeping software). Records must be accurate, complete and readable.</li>
                                                                    <li>If you’ve lost a VAT invoice or it is damaged and no longer readable, ask the supplier for a duplicate (marked ‘duplicate’).</li>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading18g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse18g" aria-expanded="false" aria-controls="collapse18g">
                                                                        <span> </span>
                                                                        <p class="red">Can I issue VAT invoices.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse18g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18g">
                                                                <div class="panel-body">
                                                                    <p>Only VAT-registered businesses can issue VAT invoices and you must:</p>
                                                                    <li>Issue and keep valid invoices - these can be paper or electronic</li>
                                                                    <li>Keep copies of all the sales invoices you issue even if you cancel them or produce one by mistake</li>
                                                                    <li>keep all purchase invoices for items you buy</li>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading19g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse19g" aria-expanded="false" aria-controls="collapse19g">
                                                                        <span> </span>
                                                                        <p class="red">Can I reclaiming VAT.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse19g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19g">
                                                                <div class="panel-body">
                                                                    <li>If you’re registered you may be able to reclaim VAT when you file your VAT Return.</li>
                                                                    <li>You can reclaim the VAT you were charged on goods and services relating to your taxable business activities.</li>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading20g">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse20g" aria-expanded="false" aria-controls="collapse20g">
                                                                        <span> </span>
                                                                        <p class="red">What percentage a VAT rates for goods and services.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse20g" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20g">
                                                                <div class="panel-body">
                                                                    <li>Standard rate 20% for most goods and services</li>
                                                                    <li>Reduced rate 5% for Some goods and services, eg children’s car seats and home energy</li>
                                                                    <li>Zero rate 0% for Zero-rated goods and services, eg most food and children’s clothes</li>

                                                                </div>
                                                            </div>
                                                        </div>
                         
                        </div>

                                 

           </div>
           <!-- end second column-->



     </div>   <!-- end row -->

        
     <br> 


     <h3 class="text-center"><a class="bg_red  buybtn" href="vatregistrationpayform.php">Register Now &nbsp; &pound; 25</a> </h3>

     
</div> <!-- end container-->


<!-- end VAT Registration  -->


<?php include 'footer.php'; ?>

