
/*-----------------------------------*/
/*  start cartaction                 */
/*-----------------------------------*/


  function cartaction(action,id,amount,package){

    var packagelist = $('#itemextra').val();
    if(action == 'add'){
      $("#add_"+id).hide();
      $("#added_"+id).show();
      var currenttotal = parseInt($("#nettotal").html());
      var newnettotal = currenttotal+parseInt(amount);
      var newtaxvat = (newnettotal*0.2);
      var newtax = (newnettotal*0.2).toFixed(2);
      var total = (newnettotal+newtaxvat).toFixed(2);

      $("#nettotal").html(newnettotal);
      $("#tax").html(newtax);
      $("#total").html(total);

      //var packagelist = $('#itemextra').val();
      if(packagelist == ""){
        var itemextra = package;
      } else {
        var itemextra = packagelist+','+package;
      }
      $('#itemextra').val(itemextra);

    } else if(action == 'remove'){
      $("#add_"+id).show();
      $("#added_"+id).hide();

      var currenttotal = parseInt($("#nettotal").html());
      var newnettotal = currenttotal-parseInt(amount);
      var newtaxvat = (newnettotal*0.2);
      var newtax = (newnettotal*0.2).toFixed(2);
      var total = (newnettotal+newtaxvat).toFixed(2);

      $("#nettotal").html(newnettotal);
      $("#tax").html(newtax);
      $("#total").html(total);

      if(packagelist == ""){
        var itemextra = '';
      } else {
        var itemextra = removeValue(packagelist, package, ',');
      }
      $('#itemextra').val(itemextra);
    }
  }

  function savecart(){

    var items = $("#orderditems").html();
    var encodedString = btoa(items);
    $('#itemdiv').val(encodedString);
      //$('#itemdiv').val(JSON.stringify(items));
    $('#totalamount').val($('#total').html());
    
    if($('#termsandcondtn').prop('checked')){
      $('#saveitems').submit();
    } else {
      $('#errtermsandcondtn').html('please select!');
    }
  }

  function removeValue(list, value, separator) {
    separator = separator || ",";
    var values = list.split(separator);
    for(var i = 0 ; i < values.length ; i++) {
      if(values[i] == value) {
        values.splice(i, 1);
        return values.join(separator);
      }
    }
    return list;
  }

/*-----------------------------------*/
/*  end cartaction                 */
/*-----------------------------------*/












