<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Company formation agent,tax, annual account,Wordpress,website hosting,cms,uk company">
    <meta name="keywords" content="company formation, company register,tax, annual account,logo design, web design, website development,web hosting,Wordpress,cms,uk company">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Almost Cost Free</title>
    <link rel="shortcut icon" href="/images/favicon.gif" type="image/x-icon" />
    <script src="https://use.fontawesome.com/8eac5fc2af.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    
    
</head>


<body>
<!-- start Menu Items -->


    <nav class="navbar"  id="scroll" >

        <div class="container-fluid">

           <!-- Logo -->
      
    
        <div class="navbar-header">

            <button type="button" class="navbar-toggle backgroundred" data-toggle="collapse" data-target="#mainNavBar">
                <span class="icon-bar bg_green"></span>
                <span class="icon-bar bg_green"></span>
                <span class="icon-bar bg_green"></span>
            </button>
            
        </div>

            
          <div class="collapse navbar-collapse" id="mainNavBar">

                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="companyformation.php">Company formation</a></li>
                    <li><a href="webdesign.php">Web Design</a></li>  
            
                    <!-- drop down menu -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Company services<span class="caret red"></span></a>
                        <ul class="dropdown-menu">

                            <li><a href="confirmationstatement.php">Confirmation statement</a></li>
                            <li><a href="vatregistration.php">VAT registration</a></li>               
                            <li><a href="companydissolved.php">Company dissolved</a></li>
                              
                        </ul>
                    </li>
                  

                    <li><a href="contactus.php">Contact us</a></li>

                    <li><a href="help.php">Help</a></li>

                    <li><a href="login.php">Log in</a></li>
                   

              


                </ul> 
            


            </div>


        </div>

    </nav>

<!-- end top header -->



