<?php include 'header.php'; ?>




  <!-- start Confirmation Statement -->

  <div class="container">

     <!-- start row--> 
     <div class="row">

                 <h3 class="red text-center">Filing Confirmation Statement</h3>

                 <hr class="col-md-12 col-xs-12"  style="border-top: 1px solid #89b416 !important; " />

                 <h3 class="text-center"><a class="bg_red  buybtn" href="confirmationstatementpayform.php">Submit Now &nbsp; &pound; 19</a> </h3>
         
                 <h3 class="green text-center">FAQ for filing confirmation statement</h3>
           
            <!-- start first column-->
            <div class="col-xs-12 col-md-6">
              

                     <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                                   

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse1f" aria-expanded="false" aria-controls="collapse1f">
                                                                        <span> </span>
                                                                        <p class="red">What is confirmation statement.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse1f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1f">
                                                                <div class="panel-body">
                                                                    <p>Every company must confirm the information that companies house hold about it is correct by delivering a confirmation statement, From 30 June 2016 the confirmation statement replaces the annual return.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse2f" aria-expanded="false" aria-controls="collapse2f">
                                                                        <span> </span>
                                                                        <p class="red">Do I need to update the information.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse2f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2f">
                                                                <div class="panel-body">
                                                                    <p>Yes,If the information Companies House hold is out of date the company must file the information needed to update its records before, or at the same time that it delivers the confirmation statement.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse3f" aria-expanded="false" aria-controls="collapse3f">
                                                                        <span> </span>
                                                                        <p class="red">how often this statement must be made in a year.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse3f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3f">
                                                                <div class="panel-body">
                                                                    <p>This statement must be made at least once a year, but the company may choose to make a statement more regularly.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse4f" aria-expanded="false" aria-controls="collapse4f">
                                                                        <span> </span>
                                                                        <p class="red">Is it a criminal offence not file your confirmation statement.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse4f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4f">
                                                                <div class="panel-body">
                                                                    <p>Yes, It’s a criminal offence to not file your confirmation statement within 14 days of the end of the review period. If you don’t do this,Companies House may prosecute the company and its officers.</p>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapse5f" aria-expanded="false" aria-controls="collapse5f">
                                                                        <span> </span>
                                                                        <p class="red">How do I make a confirmation statement.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse5f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5f">
                                                                <div class="panel-body">
                                                                    <p>A confirmation statement can be made via our software filing systems.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        

                     </div>

          </div>
          <!-- end first column-->



          <!-- start second column-->
          <div class="col-xs-12 col-md-6">
        
 
                     <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                                   
                                                
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading6f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse6f" aria-expanded="false" aria-controls="collapse6f">
                                                                        <span> </span>
                                                                        <p class="red">Do I need to submit if there is ‘no change’confirmation statement.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse6f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6f">
                                                                <div class="panel-body">
                                                                    <p>You must make a confirmation statement even if there haven’t been any changes during the review period.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading7f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse7f" aria-expanded="false" aria-controls="collapse7f">
                                                                        <span> </span>
                                                                        <p class="red">Annual fee to Companies House filed electronically.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse7f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7f">
                                                                <div class="panel-body">
                                                                    <p>The Companies House's annual fee for a confirmation statement filed electronically is £13.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading8f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse8f" aria-expanded="false" aria-controls="collapse8f">
                                                                        <span> </span>
                                                                        <p class="red">Annual fee to Companies House filed on paper.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse8f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8f">
                                                                <div class="panel-body">
                                                                    <p>The Companies House's annual fee for a confirmation statement filed on paper is £40.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading9f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse9f" aria-expanded="false" aria-controls="collapse9f">
                                                                        <span> </span>
                                                                        <p class="red">Can i do filing multiple confirmation statements in a year.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse9f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9f">
                                                                <div class="panel-body">
                                                                    <p>Companies only have to file one confirmation statement every 12 months. However, you can choose to file a confirmation statement early, or more frequently than once a year.</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading10f">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse10f" aria-expanded="false" aria-controls="collapse10f">
                                                                        <span> </span>
                                                                        <p class="red">What does confirmation date mean.?</p>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse10f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10f">
                                                                <div class="panel-body">
                                                                    <p>The date at which the company is confirming all the required information is up to date. This must be no later than 12 months after the last confirmation date.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        

                           </div>

              

            </div>
            <!-- end second column-->
            

    </div> <!-- end row -->
        
    <br> 

    <h3 class="text-center"><a class="bg_red  buybtn" href="confirmationstatementpayform.php">Submit Now &nbsp; &pound; 19</a> </h3>

     
</div> <!-- end container-->

<!-- end Confirmation Statement -->




<?php include 'footer.php'; ?>


