<?php include 'header.php'; ?>


<!-- start  Terms and conditions -->

<div class="container"> <!-- start  container-->


				   <div class="row col-xs-12"> <!-- start  row-->


							<h2 class="red">Terms and conditions</h2>

							<p>When you are browsing websites on the internet, many of these websites store tiny text files called cookies on your computer, to help track your use of the site and to personalise your journey around the website. These cookies can be stored so that if you return to a website, that website server can call the information from the cookies stored on your computer to tailor your experience of the site. </p>

							<p>Almost Cost Free Ltd is no different, we use cookies to help you shop our website more effectively and to place online orders - we do not store personally identifiable information in our cookie data . We also use some carefully selected 3rd party suppliers to enhance your online experience and they will place cookies on your computer for use on our website too. </p>

							<p>Cookies are perfectly safe to be stored on your computer and almost all web browsers have cookie storing enabled as default. However, all browsers have the option of disabling cookies being stored on your computer if you wish to do this.</p> 

							<p>Please be aware that disabling cookies on your browser will reduce your ability to shop online with www.asmostcostfree.com. We use cookies to process products in your basket and orders. Disabling your cookies will mean you cannot purchase through our website. This would also be a common experience of reduced functionality across many websites. </p>

							<h3 class="red">To learn about how change the cookie settings for your browser on our cookies &amp; your computer page. </h3>

							<h3 class="green">Cookies &amp; your computer</h3>

							<p>Cookies should be enabled by default on your browser, so if you have not changed your settings you should expect to be accepting cookies from websites. </p>

							<p>Cookies being disabled will limit your experience of www.almostcostfree.com and many other websites online. Remember, cookies are not harmful to your computer and our cookies on almostcostfree.com do not contain any personally identifiable information. If you wish to change your cookie settings we have provided a guide below for you. Please remember , if you do disable your cookies, you can allow cookies from certain approved websites, by adding the website homepage to your exceptions list. </p>

							<p>First, find out what your default web browser is on your machine, or the browser you use most often to access the internet. To work out what browser you are using, simply open the internet as usual, and then follow the steps below: <p>

							<h3 class="green">PC Users</h3>

							<p>On the tool bar at the top of your browser click 'Help' and choose the 'About' option from the drop down. This will tell you the browser type and version. </p>

							<h3 class="green">Mac Users </h3>

							<p>Open the internet as usual and once open, click on the Apple icon in the top left corner and choose the 'About' option. This will again tell you the browser type and version.</p>

							<p>Once you know the browser you are using you can find the cookie settings using the guide below relevant for your browser:</p>




							<h3 class="green">Checking your settings in Internet Explorer 8 and earlier versions</h3>

							<p>1,Locate the 'Tools' tab on the menu areas of your browser. Click on this. </p>
							<p>2,There is an option called ‘Internet Options’ on the list, select this option. </p>
							<p>3,This should open a pop up box with lots of tabbed options. Select the ‘Privacy’ tab from the list.</p> 
							<p>4,Internet Explorer has a high, medium, low auto-adjust system for internet content handling.</p> <p>You can configure your own settings by clicking the advanced tab and ticking the ‘Override automatic cookie handling’</p>
							<p>5,Settings above Medium will disable cookies, Medium or below will enable cookies</p>


							<h3 class="green">Checking your settings in Mozilla Firefox</h3>

							<p>1,Locate the 'Tools' tab on the menu areas of your browser. Click on this. </p>
							<p>2,From the list of options on the drop down menu, click on ‘Options’</p>
							<p>3,There should be a pop up window appear with tabs along the top. Find the ‘Privacy’ tab and select this.</p>
							<p>4,You should now have some options for ‘Tracking’ and ‘History’. Under the ‘History’ drop down menu, select ‘Use customer settings for history’ if you wish to adjust your cookie settings.</p>

							<h3 class="green">Checking your settings in Google Chrome</h3>

							<p>1,Look for the spanner icon in Chrome  and click on this</p>
							<p>2,From this menu, select settings…</p>
							<p>3,Within settings you will see tabs on the left hand side. Select the tab called ‘Under the Hood’</p>
							<p>4,When this page loads you will see a tab at the top called ‘Content Settings’ – select this</p>
							<p>5,This brings up your content controls including cookie permissions – select the option you feel  most comfortable with</p>
								

							<h3 class="green">Checking your settings in Safari</h3>

							<p>1,Locate the Cog icon at the top of your browser  and click on this.</p>
							<p>2,From the list of options select ‘Preferences’</p>
							<p>3,This should open a pop up window with several tabs along the top. From this list select the ‘Privacy’ option. </p>
							<p>4,This will open the tab page that allows you to control the cookie settings on your browser.</p>

							<h3 class="green">Checking your settings in Opera</h3>

							<p>1,Click the Opera file icon in the top left hand corner of your browser. </p>
							<p>2,From the drop down list of options, highlight ‘Settings’ and then scroll across and select ‘Preferences’.</p>
							<p>3,This should open a pop up window with tabs along the top. Choose the ‘Advanced’ tab. </p>
							<p>4,This shows you the advanced options. On the left hand side there is an option called ‘Cookies’ – click on this option. </p>
							<p>5,This presents the cookie options for your use on this browser.</p>

							<p>You can visit the following website for an in-depth guide to cookies and how to control, delete and understand them. They even have a great recipe on there for cookies of the edible variety – yum! http://www.aboutcookies.org/</p>
							 
							<p>Recent legislation from the European Union has meant that websites must provide clear information about their use of cookies to their customers, something we at Almost Cost Free Ltd fully support. We want to ensure that you, as our valued customer, are fully aware of the use of cookies on our website, and we are proud of our reputation as a transparent and honest services provider that you can trust.</p>

				</div> <!-- end  row-->

</div> <!-- end  container-->

<!-- end  Terms and conditions -->




<?php include 'footer.php'; ?>


